#ifndef __GSM_PORT_H__
#define __GSM_PORT_H__

#define GSM_DISABLE()
#define GSM_ENABLE()

#define FOSC 	24000000L         
#define BAUD 	9600           
#define GSM_RX_BUF_SIZE sizeof(msg_recv_buf)

extern unsigned char xdata Rec_point;
extern unsigned char msg_recv_buf[64];

void GSM_UART_INIT(void);
void GSM_WriteOneByte(unsigned char dat);
void GSM_WriteString(char *p);



#endif