#ifndef __OS_API_H__
#define __OS_API_H__

#include "rtx51tny.h"
#include <STC15F2K60S2.H>

#define os_sched_stop()			ET0 = 0
#define os_sched_resume()		ET0 = 1

void OS_DELAY(unsigned long);
#endif