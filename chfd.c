#include"chfd.h"
#include 	<config.h> 
#include <sim900a.h>
unsigned int  xdata vol_temp=0;//电瓶电压值
unsigned long xdata high_vol=0;
unsigned int xdata stop_vol=8000;//停机电压值
unsigned int xdata bj_vol=6500;//报警电压值
unsigned int xdata chd_base_vol=0;
unsigned int xdata chd_old_vol=0;
unsigned int xdata chd_pre_vol=0;
unsigned char xdata kd_vol=70;//匮电值
float xdata temp_f_vol=0;
unsigned char xdata chfa_num=0;//放电次数
unsigned int xdata fd_count=0;//放电计数器
unsigned int xdata ych_val=500;//压差值
unsigned char xdata err_code=0;
unsigned char xdata Hf_Code=0;
unsigned char xdata fs_on_count=0;
extern unsigned char Chd_5min_Count;
bit Fd_Disp_Fg=0,fs_on_flag=0;//开启风扇;
bit cm_flag,Call_Scu_Fg=0;
bit run_flag=0;
bit on_flag=0;
bit chd_jy_fg;
bit off_flag=1;
bit fina_fd_flag=0;
bit time_off_flag=0;
bit t_call_flag;
bit yx_flag=0;//压线标志
bit kd_flag=0;//匮电标志
bit fdr_flag=1;//防电人标志
bit fd_flag=0;//放电标志
bit chd_flag=0;//充电标志
bit chd_min_flag=0;//充电一分钟到标志
bit fd_fail=0;
bit chd_5_min_flag=0;
bit err_flag=0,call_flag=0;
// bit sms_hf_flag=0;
bit sms_fd_flag;
bit gsm_time_on_flag=0;
bit gsm_time_off_flag=0;
extern bit Gsm_Tx_Fg,bj_off_flag;
extern bit set_flag;//设置标志
extern bit Call_Fg;
extern bit Rec_Sms_Fg;
extern bit  bj_start_flag;
extern bit x_err;
extern bit Sms_Hf_Fg;
extern unsigned  char xdata Smg_Disp_Two;//第二组数码管显示指针
extern unsigned  char xdata Smg_Disp_Four;//第四组数码管显示指针
/******************************************************************************************
** 函数名称 ：fd_pro()函数
** 函数功能 ：放电处理
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/
void fd_pro(void)
{
	
	if(high_vol<=30 )
	{
		if(high_vol==0)
		{
			
      if(sms_fd_flag)	
			 {		
        sms_fd_flag=0;
        if(!x_err) //gsm模块正常工作则回复短信，否则不回复短信	
				{					
			  Hf_Code=0;				 
			  Gsm_Tx_Fg=1;	
				}					
			 }
// 			 safe_led=1;
// 			 bj_out=0;//关闭蜂鸣器
//  			 bj_off_flag=0;
			 fd_fail=0;			 
			 fd_count=0;
			 fd_flag=0;
			 fina_fd_flag=1;
			 Smg_Disp_Four=8;
		}
// 		else 	
// 		{
// 			bj_out=0;//关闭蜂鸣器
// 		  bj_off_flag=0;
// // 			 safe_led=1;
// 		}
	}

			else 
			{
			
				if(fd_count>400)
				{				
				fd_count=0;
				fd_fail=1;
				fina_fd_flag=0;
				fd_flag=0;	
					if(!x_err) //gsm模块正常工作则回复短信，否则不回复短信	
					{						
					 Hf_Code=0;
					 Gsm_Tx_Fg=1;	
					}
			  }
			}
}
/******************************************************************************************
** 函数名称 ：chd_pro()函数
** 函数功能 ：充电处理
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/
void chd_pro(void)
{
	if(chd_5_min_flag)
	{
		chd_5_min_flag=0;
	if(high_vol>=stop_vol)
			{
				 chd_flag=0;//充电模清除
						Chd_5min_Count=0;
// 						chd_min_flag=0;
					jdq=1;//关闭充电
					cm_flag=1;//充满标志置1
					err_flag=0;
					err_code=0;
					yx_led=1;
					yx_flag=0;
			 }
		else
			{
					chd_flag=0;//充电模清除
// 						chd_min_flag=0;
					Chd_5min_Count=0;
					jdq=1;//关闭充电
					err_flag=1;
				if(!x_err) //gsm模块正常工作则回复短信，否则不回复短信	
				{	
					Gsm_Tx_Fg=1; 
					Hf_Code=1;
				}
					err_code=1;				
					yx_led=0;
					yx_flag=1;
					on_flag=0;
					off_flag=1;//关机模式
		}
	}
// 	if(chd_min_flag)
// 	{
// 		chd_min_flag=0;
// 			if(high_vol>=1500+chd_old_vol)
// 			{
// 				chd_old_vol=high_vol;
// 				chd_pre_vol=high_vol;
// 			}
// 			else
// 			{
// 				chd_flag=0;//充电模清除
// 				jdq=1;//关闭充电
// 				err_flag=1;
// 				if(!x_err) //gsm模块正常工作则回复短信，否则不回复短信	
// 				{	
// 					Gsm_Tx_Fg=1; 
// 					Hf_Code=1;
// 				}
// // 				Gsm_Tx_Fg=1;
// 				err_code=1;
// 				Hf_Code=err_code;
// 				on_flag=0;
// 		    off_flag=1;//关机模式
// 				yx_led=0;
// 				yx_flag=1;
// 			}

// 	}
// else
// {
// 		if(high_vol>=1500+chd_old_vol)
// 			{
// 				chd_old_vol=high_vol;
// 				chd_pre_vol=high_vol;
// 				chd_min_flag=0;
// 			}
// 	
// }
}
// unsigned char xdata Hf_Code;
/******************************************************************************************
** 函数名称 ：Task_Main()函数
** 函数功能 ：主任务处理
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
***************************************************************************************/
void Task_Main(void)
{
	 if(Call_Scu_Fg&&!x_err&&!bj_start_flag) 
	 {
		 Call_Scu_Fg=0;
		 Hf_Code=8;
		 Gsm_Tx_Fg=1;
		 chd_base_vol=high_vol;
	 }
	 if(Call_Scu_Fg&&x_err) 
	 {
		 Call_Scu_Fg=0;
	 }	 
// 	 if((gsm_time_on_flag||gsm_time_off_flag)&&!x_err)  
// 	 {
// 		 Hf_Code=0;
// 		 Gsm_Tx_Fg=1;
// 	 }
if(t_call_flag&&on_flag&&!x_err)
{
	Call_Fg=1; 
	t_call_flag=0;
	Call_Scu_Fg=0;
}
// 	 if(on_flag)Call_Fg=1; 
// 	 on_flag=0;
if(t_call_flag &&!on_flag) t_call_flag=0; 
if(t_call_flag&&on_flag&&x_err)t_call_flag=0;
if(err_flag&&!set_flag)Smg_Disp_Two=1;
if(!err_flag&&!set_flag)Smg_Disp_Two=0; 
if(on_flag&&!set_flag) Smg_Disp_Four=7;
if(hand("+CMTI")&&!Sms_Hf_Fg)	
{	
	Rec_Sms_Fg=1;//若缓存字符串中含有"+CMT"就表示有新的短信
}
if(chd_flag) chd_pro();//若在充电模式调充电处理程序
else
{
		chd_min_flag=0;
	  chd_5_min_flag=0;	
	  Chd_5min_Count=0;
}
if(fd_flag)	 fd_pro();//若在放电模式调放电处理程序	 
if(vol_temp<kd_vol&&err_code!=2)	 
{
	err_flag=1;
  err_code=2; 
	on_flag=0;
	off_flag=1;//关机模式
	chd_flag=0;//充电模清除
	Chd_5min_Count=0;
	jdq=1;//关闭充电	
	low_vol_led=0;
	kd_flag=1;
	if(err_code==6)
	{
		err_flag=0;
		err_code=0; 
		if(!set_flag)
		Smg_Disp_Two=0;//第二个数码管显示E--X
	}
}
else if(vol_temp>=kd_vol&&vol_temp<=170)	
{
	if(err_code==2|| err_code==6)
	{
		err_flag=0;
		err_code=0; 
		if(!set_flag)
		Smg_Disp_Two=0;
	}
	low_vol_led=1;
	kd_flag=0;
}
else if(vol_temp>170&&err_code!=6)	 
{
	err_flag=1;
  err_code=6; 
	chd_flag=0;//充电模清除
	Chd_5min_Count=0;
	jdq=1;//关闭充电	
	if(err_code==2)
	{
	err_flag=0;
  err_code=0;
	if(!set_flag)
  Smg_Disp_Two=0;		
	}
}
if(temp_f_vol<40&&!fs_on_flag)
{
	fs_out=0;//关闭风扇
if(err_code==4)
	{
	err_flag=0;

  err_code=0; 	
	if(!set_flag)Smg_Disp_Two=0;
	}
}
else  if(temp_f_vol>45)
{
	fs_out=1;//开启风扇
	if(err_code==4)
	{
	err_flag=0;
  err_code=0; 
	if(!set_flag)Smg_Disp_Two=0;
	}
}
else if(temp_f_vol>80&&err_code!=4 )
{
	fs_out=1;//开启风扇
	err_flag=1;
// 	sms_hf_flag=1;
  err_code=4;
	if(!x_err) //gsm模块正常工作则回复短信，否则不回复短信	
				{	
					Gsm_Tx_Fg=1; 
					Hf_Code=4;
				}	
	if(!set_flag)Smg_Disp_Two=1;	
// 	hf_sms_pro(err_code);
}
if(safe_in&&fdr_flag&&on_flag)
	{
			on_flag=0;
		  fdr_flag=0;
		  safe_led=1;
			off_flag=1;//关机模式
			fd_flag=1;
			cm_flag=0;//充满标志置1
			fd_count=0;
			fina_fd_flag=0;
		  fs_out=1;//开启风扇
		  fs_on_flag=1;
		  fs_on_count=0;
			fd_ctrl=0;//放电
			chd_flag=0;//充电模清除
		  	Chd_5min_Count=0;
			chd_min_flag=0;
			jdq=1;//关闭充电
		  bj_off_flag=0;
// 			err_flag=0;
// 			err_code=0; 
		err_flag=1;
// 	sms_hf_flag=1;
  err_code=7;
	if(!x_err) //gsm模块正常工作则回复短信，否则不回复短信	
				{	
					Gsm_Tx_Fg=1; 
					Hf_Code=7;
				}	
			if(!set_flag)Smg_Disp_Two=1;	
		  Fd_Disp_Fg=1;	
		  Smg_Disp_Four=0;
// 			 Gsm_Tx_Fg=1;			
	}
if(on_flag)
{

				if(high_vol<bj_vol)			
				{
				if(!bj_off_flag)
				bj_out=1;//打开蜂鸣器
				}					
			else bj_out=0;//打开蜂鸣器
				
			if(high_vol<bj_vol&&cm_flag)
			{
				cm_flag=0;
				if(!chd_jy_fg)
				{
					chd_pre_vol=high_vol;
					chd_jy_fg=1;
				}	
				if(chfa_num>=0xff)chfa_num=0xff;
				else	chfa_num++;
				fina_fd_flag=0;
// 				chd_base_vol=high_vol;
				chd_old_vol=high_vol;
// 				chd_pre_vol=high_vol;
				jdq=0;//开始充电
// 				bj_off_flag=0;
			
				bj_start_flag=1;
// 				cm_flag=0;//充满标志置1
				t_call_flag=1;
				if(err_code==3)
				{
					err_flag=0;
					err_code=0; 
					if(!set_flag)Smg_Disp_Two=0;	
				}
			}	
			else if(high_vol<stop_vol-ych_val)
			{
				chd_flag=1;		      		    
				fina_fd_flag=0;
        if(!chd_jy_fg)
				{
					chd_pre_vol=high_vol;
					chd_jy_fg=1;
				}					
//       	bj_out=0;//打开蜂鸣器
				jdq=0;//开始充电		      
				if(err_code==3)
				{
					err_flag=0;
					err_code=0; 
					if(!set_flag)Smg_Disp_Two=0;					
				} 
			}
			else if(high_vol>=stop_vol )
			{
				chd_flag=0;//充电模清除
				chd_min_flag=0;
				chd_5_min_flag=0;
				Chd_5min_Count=0;
				jdq=1;//关闭充电
				cm_flag=1;//充满标志置1
				chd_jy_fg=0;
// 				bj_off_flag=0;
				bj_out=0;//关闭蜂鸣器
				err_flag=0;
				err_code=0;
				yx_led=1;
				yx_flag=0;
				if(err_code==3)
				{
					err_flag=0;
					err_code=0; 
					if(!set_flag)Smg_Disp_Two=0;	
				}
			}
		}
		else 	
		{
			chd_jy_fg=0;
			bj_out=0;//打开蜂鸣器
		}
 }