#ifndef __TM1640_H__
#define __TM1640_H__


#define PCLK	P43
#define PDIO 	P42

extern uint8_t vDisBuf[16];
void TM1640_Init(void);
void TM1640_Refresh(void);
#endif