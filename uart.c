#include "uart.h"
#include "STC15F2K60S2.H"
#include "user_config.h"

#define URMD	1//选择定时器1，为串口提供时钟。
#define Baudrate		9600
#define CalBaudrate1T(X)					(65536UL-Fosc/4/(X))
/*----------------------------
初始化串口
----------------------------*/
void Uart_Init(void)
{
    SCON = 0x5a;                //设置串口为8位可变波特率
#if URMD == 0
    T2L = 0xd8;                 //设置波特率重装值
    T2H = 0xff;                 //115200 bps(65536-18432000/4/115200)
    AUXR = 0x14;                //T2为1T模式, 并启动定时器2
    AUXR |= 0x01;               //选择定时器2为串口1的波特率发生器
#elif URMD == 1
    AUXR |= 0x40;                //定时器1为1T模式
		AUXR &= ~0x01;//定时1作为波特率发生器
	
    TMOD &= 0x0f;                //定时器1为模式0(16位自动重载)
    TL1 = CalBaudrate1T(Baudrate);		//设置波特率重装值
    TH1 = CalBaudrate1T(Baudrate)>>8;		//115200 bps(65536-18432000/4/115200)
    TR1 = 1;                    //定时器1开始启动
#else
    TMOD = 0x20;                //设置定时器1为8位自动重装载模式
    AUXR = 0x40;                //定时器1为1T模式
    TH1 = TL1 = 0xfb;           //115200 bps(256 - 18432000/32/115200)
    TR1 = 1;
#endif

	ES = 1;  //使能串口中断
}

/*----------------------------
发送串口数据
----------------------------*/
unsigned char Uart_SendByte(unsigned char d)
{
    while (!TI);                    //等待前一个数据发送完成
    TI = 0;                         //清除发送标志
    SBUF = d;                     //发送当前数据
		return d;
}



void Uart_SendByteN(unsigned char *d, unsigned short len)
{
	unsigned short i = 0;
	for(i=0;i<len;i++){
		Uart_SendByte(d[i]);
	}
}


void Uart_PrintHex(unsigned char a){
	unsigned char t;
	t = a>>4;
	if(t<10)
		t = t+'0';
	else
		t = t-10+'A';
	Uart_SendByte(t);
	
	t = a&0xf;
	if(t<10)
		t = t+'0';
	else
		t = t-10+'A';
	Uart_SendByte(t);
}