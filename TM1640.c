
//参考：https://wenku.baidu.com/v/1df5a9bef01dc281e43af01e.html
#include "TM1640.h"
#include "STC15F2K60S2.H"//单片机头文件,24MHz时钟频率

#include "c_types.h"
#include "user_config.h"
#include "os_api.h"

uint8_t vDisBuf[16]={\
	0xff,0xff,0xff,0xff,\
	0xff,0xff,0xff,0xff,\
	0xff,0xff,0xff,0xff,\
	0xff,0xff,0xff,0xff,\
};

static void RefreshScreen() _task_ TASK_ID_RefreshScreen {
	while(1){
		os_wait1(K_SIG);
		os_clear_signal(K_SIG);
		
		Start_1640();
		NOP();
		NOP();
		Write_1640(0xc0);    //显示寄存器的00H单元开始 
		NOP();
		for(j= 0; j< sizeof(vDisBuf); ++j)
		{
			 Write_1640(vDisBuf[j]);
		}
		Stop_1640();
		
	}
}

static void NOP(void)  
{     
	int T_Dly = 4;
	while(T_Dly--);           
	return ;  
}  
/*--TM1640数码管驱动IC程序------------------------*/
//*********START***********************************************
void Start_1640()          //1640开始
{
    PCLK=0;
    PDIO=1; 
    PCLK=1;    
    NOP();
    NOP(); 
    PDIO=0;
    NOP();
    NOP();
    PCLK=0;
    NOP();
    NOP();
}
//**************************************************************
void Stop_1640()             //1640结束
{
    PCLK=0;
    PDIO=0;
    PCLK=1;
    NOP();
    NOP(); 
    PDIO=1;
    NOP();
    NOP();
}


/*------写数据给T1640----------*/
void Write_1640(uint8_t vdata1)
{
		uint8_t i = 0;
		PDIO=0;
		PCLK=0;
		NOP();
		NOP();
		for(i=0;i<8;i++)  //开始传送8位数据，每循环一次传送一位数据
		{
			PCLK=0;
			NOP();
			NOP();
			if(vdata1&0x01){
				PDIO=1;
			}
			else{
				PDIO=0;
			}
			NOP();
			PCLK=1;
			NOP();
			NOP();
			vdata1>>=1;
		}
		PDIO=0;
		PCLK=0;
}


/*------显示子程序--------*/
void TM1640_Init(void)    
{
		uint8_t j=0;
	
		/*
		<PxM1.n,PxM0.n> = :
		00->Standard,
		01->Pull-Push,
		10->Input,
		11->Open Drain.
		*/
		P4M0|=(1<<3)|(1<<2);
		P4M1&=~((1<<3)|(1<<2));
		
	os_sched_stop();
	
    NOP();
    
		Start_1640();
		NOP();
		NOP();
		Write_1640(0x40);    //写数据到显示寄存器，采用地址自动加一
		NOP();
		Stop_1640();
		Start_1640();
		NOP();
		NOP();
		Write_1640(0xc0);    //显示寄存器的00H单元开始 
		NOP();
		for(j= 0; j< sizeof(vDisBuf); ++j)
		{
			 Write_1640(vDisBuf[j]);
		}
		Stop_1640();
		Start_1640();
		NOP();
		NOP();
		Write_1640(0x8e);     //开显示，亮度可以通过改变低三位调节  0x8e--10001110
		NOP();
		NOP();
		Stop_1640();
   os_sched_resume();
}



void TM1640_Refresh(void){
	os_send_signal(TASK_ID_RefreshScreen);
}