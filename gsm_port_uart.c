#include "gsm_common.h"
#include "gsm_port.h"
#include "uart.h"
#include "rtx51tny.h"

unsigned char xdata Rec_point=0;//串口接收位置
unsigned char msg_recv_buf[64];


static bit busy = 0;
/******************************************************************************************
** 函数名称 ：UART_init
******************************************************************************************/	
void GSM_UART_INIT (void)	  
{
	Uart_Init();//BAUD
}
/******************************************************************************************
** 函数名称 ：串口接收中断函数
******************************************************************************************/	
void Uart() interrupt 4 using 1
{
    if (RI)
    {
        RI = 0; 
				msg_recv_buf[Rec_point] = SBUF; 
				Rec_point++;
				if(Rec_point>GSM_RX_BUF_SIZE)          
				{
					Rec_point = 0;
				}
    }
		
    if (TI)
    {
        TI = 0;                 
        busy = 0;             
    }
}

/******************************************************************************************
** 函数名称 ： GSM_WriteOneByte()函数
** 函数功能 ：向gsm模块发送一个字节
******************************************************************************************/	
void GSM_WriteOneByte(unsigned char dat)
{
    while (busy);
    busy = 1;
    SBUF = dat;
}
