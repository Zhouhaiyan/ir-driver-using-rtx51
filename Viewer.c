#include 	<stdlib.h> 
#include 	<stdio.h>
#include	<string.h>
#include "apl.h"
#include "Key.h"
#include "TM1640.h"


/* Includes ------------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void config_sm(uint32_t k, uint8_t ena);
void CaputureInput(void);
/* Private functions ---------------------------------------------------------*/

//todo:ViewScreen1~4,显示重叠，未处理
static void FlashPwrLedTask() _task_ TASK_ID_FlashPwrLed {
	while(1){
		//注意：只刷新显示缓冲区即可，刷新屏幕交给任务TASK_ID_RefreshScreen来做。
		//todo:关机指示灯,亮
		//...
		TM1640_Refresh();
		OS_DELAY(200);
		//todo:关机指示灯,灭
		//...
		TM1640_Refresh();
		OS_DELAY(200);
		
		if(app.m.power != PWR_OFF)//过热会导致关机。再次开机后，又会自动删除电源灯快闪任务。
			os_delete_task(TASK_ID_FlashPwrLed);//删除电源灯快闪任务
	}
}
static Event_t ViewDefault(Event_t e){
	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_Beep){
		//OPC_SET:蜂鸣器叫
		return e^EVT_MASK_Beep;
	}
	//todo :还有7个指示灯逻辑。可能导致32位事件不够用
	
	if(e&EVT_MASK_SwScreen){
		//响应屏幕开关
		if(app.m.screen_switch == 0){
			memset(vDisBuf,0,sizeof(vDisBuf));//清空屏幕显示
			TM1640_Refresh();
		}
		return e^EVT_MASK_SwScreen;
	}
	
	if(e&EVT_MASK_OverHeat){
		//关机指示灯快闪，提示过热
		os_create_task(TASK_ID_FlashPwrLed);//创建电源灯快闪任务
		return e^EVT_MASK_SwScreen;
	}
	
	return e;
}


static Event_t ViewScreen1(Event_t e){
	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_BatteryVolt){
		return e^EVT_MASK_BatteryVolt;
	}
	if(e&EVT_MASK_ShockCounter){
		return e^EVT_MASK_ShockCounter;
	}
	if(e&EVT_MASK_HighVolt){
		return e^EVT_MASK_HighVolt;
	}
	return e;
}

static Event_t ViewScreen2(Event_t e){
	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_Temperature){
		return e^EVT_MASK_Temperature;
	}
	return e;
}

static Event_t ViewScreen3(Event_t e){
	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_Timing){
		return e^EVT_MASK_Timing;
	}
	return e;
}

static Event_t ViewScreen4(Event_t e){

	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_CFG1){
		return e^EVT_MASK_CFG1;
	}
	if(e&EVT_MASK_CFG2){
		return e^EVT_MASK_CFG2;
	}
	if(e&EVT_MASK_CFG3){
		return e^EVT_MASK_CFG3;
	}
	if(e&EVT_MASK_CFG4){
		return e^EVT_MASK_CFG4;
	}
	if(e&EVT_MASK_CFG5){
		return e^EVT_MASK_CFG5;
	}
	if(e&EVT_MASK_CFG6){
		return e^EVT_MASK_CFG6;
	}
	if(e&EVT_MASK_CFG7){
		return e^EVT_MASK_CFG7;
	}
	if(e&EVT_MASK_CFG8){
		return e^EVT_MASK_CFG8;
	}
	if(e&EVT_MASK_CFG9){
		return e^EVT_MASK_CFG9;
	}
	return e;
}

static Event_t ViewGSM(Event_t e){
	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_GSM_Bind){
		return e^EVT_MASK_GSM_Bind;
	}
	if(e&EVT_MASK_GSM_Unbind){
		return e^EVT_MASK_GSM_Unbind;
	}
	if(e&EVT_MASK_Power){
		return e^EVT_MASK_Power;
	}
	if(e&EVT_MASK_AntiShock){
		return e^EVT_MASK_AntiShock;
	}
	if(e&EVT_MASK_SwScreen){
		return e^EVT_MASK_SwScreen;
	}
	if(e&EVT_MASK_SwTiming){
		return e^EVT_MASK_SwTiming;
	}
	if(e&EVT_MASK_GSM_Inquiry){
		return e^EVT_MASK_GSM_Inquiry;
	}
	if(e&EVT_MASK_CFG1){
		return e^EVT_MASK_CFG1;
	}
	if(e&EVT_MASK_CFG2){
		return e^EVT_MASK_CFG2;
	}
	if(e&EVT_MASK_CFG3){
		return e^EVT_MASK_CFG3;
	}
	if(e&EVT_MASK_CFG4){
		return e^EVT_MASK_CFG4;
	}
	if(e&EVT_MASK_CFG5){
		return e^EVT_MASK_CFG5;
	}
	if(e&EVT_MASK_CFG6){
		return e^EVT_MASK_CFG6;
	}
	if(e&EVT_MASK_CFG7){
		return e^EVT_MASK_CFG7;
	}
	if(e&EVT_MASK_CFG8){
		return e^EVT_MASK_CFG8;
	}
	if(e&EVT_MASK_CFG9){
		return e^EVT_MASK_CFG9;
	}
	
	
	if(e&EVT_MASK_Shock){
		return e^EVT_MASK_Shock;
	}
	if(e&EVT_MASK_TouchLine){
		return e^EVT_MASK_TouchLine;
	}
	if(e&EVT_MASK_OverCurrent){
		return e^EVT_MASK_OverCurrent;
	}
	if(e&EVT_MASK_BatteryLow){
		return e^EVT_MASK_BatteryLow;
	}
	if(e&EVT_MASK_OverHeat){
		return e^EVT_MASK_OverHeat;
	}
	if(e&EVT_MASK_AntiShock){
		return e^EVT_MASK_AntiShock;
	}
	
	return e;
}


/**
  * @brief  This function handles Viewer events.
  * @param  None
  * @retval None
  */
void PollV(void)
{
	#define eVGSM				(app.v.msgGSM)
	#define eVDefault 	(app.v.msg)
	#define eVScreen1 	(app.v.msgScreen1)
	#define eVScreen2 	(app.v.msgScreen2)
	#define eVScreen3		(app.v.msgScreen3)
	#define eVScreen4		(app.v.msgScreen4)
	#define CurView			(app.v.CurView)
	/* View GSM -------------------------------------------------------------- */
	if(CurView & GSMView)
		eVGSM = ViewGSM(eVGSM);
	
	/* View Screen1 ----------------------------------------------------------- */
	if(CurView & Screen1View)
		eVScreen1 = ViewScreen1(eVScreen1);
	
	/* View Screen2 ----------------------------------------------------------- */
	if(CurView & Screen2View)
		eVScreen2 = ViewScreen2(eVScreen2);
	
	/* View Screen3 ----------------------------------------------------------- */
	if(CurView & Screen3View)
		eVScreen3 = ViewScreen1(eVScreen3);
	
	/* View Screen4 ----------------------------------------------------------- */
	if(CurView & Screen4View)
		eVScreen4 = ViewScreen4(eVScreen4);
	
	/* Default View ----------------------------------------------------------- */
	eVDefault = ViewDefault(eVDefault);
	
//	CaputureInput();//检测机械按键、红外按键、GSM指令。
}






void CaputureInput(void)
{
	uint32_t t;
	t = TakeKey();
	if(t & KeyID_PwrSaving){
		app.c.msg |= EVT_MASK_Power;
		app.msg_buf[EVT_Power][0] = OPC_ASSIGN;
		app.msg_buf[EVT_Power][1] = PWR_SAVING;
	}
	if(t & KeyId_AntiShock){
		app.c.msg |= EVT_MASK_AntiShock;
		app.msg_buf[EVT_AntiShock][0] = OPC_TGL;
	}
	if(t & KeyId_Pwr){
		app.c.msg |= EVT_MASK_Power;
		app.msg_buf[EVT_Power][0] = OPC_TGL;
	}
	
	if(t & KEY_MSK_SwBuzzer){
		app.c.msg |= EVT_MASK_SwBuzzer;
		app.msg_buf[EVT_SwBuzzer][0] = OPC_TGL;
	}
	if(t & KEY_MSK_SwTiming){
		app.c.msg |= EVT_MASK_SwTiming;
		app.msg_buf[EVT_SwTiming][0] = OPC_TGL;
	}
	if(t & KEY_MSK_SwScreen){
		app.c.msg |= EVT_MASK_SwScreen;
		app.msg_buf[EVT_SwScreen][0] = OPC_TGL;
	}
	if(t & KEY_MSK_ClrTiming){
		app.c.msg |= EVT_MASK_ClrTiming;
	}
	
	config_sm(t,1);//轮询“红外-数显”设置界面状态机
}




static void config_sm(uint32_t k, uint8_t ena){
	static uint8_t s = 0;
	
	if(!ena)
		s = 0;//如果ena=0,复位状态机。
	
	switch(s){
		case 0:
			if(k & KEY_MSK_Menu){
				s = 1;//进入鉴权
			}
			break;
		case 1:
			//显示鉴权提示
		
			//接收红外密码
		
			if(1){//鉴权成功
				s = 2;//进入设置界面
			}
			else{//鉴权失败
				s = 0;//退出设置
			}
			break;
		case 2:
			if(k & KEY_MSK_Menu){
				//切换数值SE1~SE7
			}
			else if(k & KEY_MSK_Inc){
				//数值+
			}
			else if(k & KEY_MSK_Dec){
				//数值-
			}
			else if(k & KEY_MSK_Confirm){
				//@todo: 播放确认提示
				s = 0;
			}
			else if(k & KEY_MSK_ESC){
				s = 0;
			}
			break;
	}
}