#include <adc.h>

unsigned  int  code tempresist[]={28709,27421,26200,25042,23943,22899,21908, 20967, 20072, 19221, 18412, 17642, 16910,16213, 15549,14916,14314,13740,13192,12670,12172, 11696,11243, 10809, 10395,10000,9622,9261, 8916, 8585, 8269, 7967,7677,7400,7135,  
6880, 6637, 6403, 6179, 5964, 5758, 5561, 5371, 5189, 5014, 4846, 4685, 4530, 4381,
	4238, 4101, 3968, 3841, 3719,  
3601, 3487, 3378, 3273, 3172, 3074, 2980, 2890, 2803, 2718, 2637, 2559, 2483, 2411, 2340, 2272, 2207, 2144, 2082, 2023, 1966, 1911, 1858, 1807, 1757, 1709, 1662, 1617, 1573, 1531, 1491, 1451, 1413,  
1376, 1340, 1305, 1271, 1239, 1207, 1177, 1147, 1118, 1090, 1063,1037, 1011,987,963,939,917,895,874,853,833,813,794,776,758 
};
uchar  Degree_C=0,Degree_0C=0;//温度值
uint xdata vol_cal_temp[6]={0,0,0,0,0,0};
uint xdata vol_cal_temp1[5]={0,0,0,0,0};
bit start_disp_fg;
extern uint xdata vol_adj;
extern unsigned int xdata vol_temp;//电瓶电压值
extern unsigned long xdata high_vol;
extern uint xdata t_test;
extern float xdata temp_f_vol;
void InitADC();
unsigned long GetADCResult(unsigned char ch);
uint AD_Filter(uint* pData,uchar N);
/*----------------------------
读取ADC结果
----------------------------*/
unsigned long  GetADCResult(unsigned char ch)
{
    ADC_CONTR = ADC_POWER | ADC_SPEEDLL | ch | ADC_START;
    _nop_();                        //等待4个NOP
    _nop_();
    _nop_();
    _nop_();
    while (!(ADC_CONTR & ADC_FLAG));//等待ADC转换完成
    ADC_CONTR &= ~ADC_FLAG;         //Close ADC
   ADC_RESL=ADC_RESL&0x03;
//    ntcdianya=
    return (ADC_RES*4+ADC_RESL);                //返回ADC结果
}
/*----------------------------
初始化ADC
----------------------------*/
void InitADC()
{
    P1ASF = 0x0f;                   //设置P1.0~P1.口为AD口
    ADC_RES = 0;                    //清除结果寄存器
    ADC_CONTR = ADC_POWER | ADC_SPEEDLL;
//     Delay(2);                       //ADC上电并延时
}

/******************************************************************************************
** 函数名称 ：NTCresist()函数
** 函数功能 ：ntc温度检测
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/
uint NTCresist(void)
{
	unsigned long xdata cal_temp;
	unsigned long xdata tem_temp;
	uint res_temp;
	  tem_temp=(GetADCResult(1)*5000/1024);
  	 cal_temp=5100*tem_temp;
  	 tem_temp=(5000-tem_temp);
	   res_temp=cal_temp/tem_temp;
	   return res_temp;
// 	   res_temp=Compare_tempres(res_temp);
}
unsigned char Compare_tempres(unsigned int TR)
{ 
 unsigned char cmp_cnt; 
 cmp_cnt =0; 
 while (TR<tempresist[cmp_cnt]) 
 { 
  cmp_cnt++; 
  if (cmp_cnt>140) 
  break; 
 } 
 return cmp_cnt; 
} 
//检测温度
void ADC_Temperature(void) 
{ 
  uint xdata NTCvalue=0;
  NTCvalue=NTCresist(); 
	Degree_C = Compare_tempres(NTCvalue);
	Degree_0C= ((tempresist[Degree_C-1]-NTCvalue)*10)/(tempresist[Degree_C-1]-tempresist[Degree_C]);	
	temp_f_vol=Degree_C*10+Degree_0C;
	temp_f_vol=temp_f_vol/10.0;
}

