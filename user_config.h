#ifndef __USER_CONFIG_H__
#define __USER_CONFIG_H__
//INT_CLOCK = 10K, Systick = INT_CLOCK/Fosc = 2ms
#define	Fosc				20000000UL		// 定时基准xxms,特别注意不能超过当前主频下的最大定时值
#define SysTickMS		2

#define USE_OS	1

#if USE_OS

#define TASK_ID_InverterTask	4
#define TASK_ID_FlashPwrLed		5
#define TASK_ID_RefreshScreen	6

#endif
#endif