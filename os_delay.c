#include "user_config.h"
#include "rtx51tny.h"

void OS_DELAY(unsigned long a){
#if 0
	unsigned char i;
	i = (unsigned char)((a) >> 8);
	while(i--){
		os_wait(K_IVL,255/SysTickMS,0);
	}
	i = (unsigned char)(a);
	os_wait(K_IVL,i/SysTickMS,0);
#else
	unsigned long i;
	i = (unsigned long)((a) >> 8);
	while(i--){
		os_wait(K_IVL,255/SysTickMS,0);
	}
	i = (unsigned char)(a);
	os_wait(K_IVL,i/SysTickMS,0);
#endif
}

