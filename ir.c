#include "STC15F2K60S2.H"
#include "user_config.h"
#include "ir.h"
#include "c_types.h"




#define RELOAD_VALUE_MS		20
#define	RELOADMS(T)				(unsigned int)(65535-(Fosc*T/12/1000))		// 12T模式。定时器初值
#define CNT2US100(C)			(unsigned int)((65535UL-(C))*12UL/(Fosc/1000UL/10))

static uint8_t ir_key = 0;		//红外按键码值
static uint8_t tout_flag = 0;	//NEC码超时标志
static uint8_t tbuf[40];			//记录NEC码时间
static uint8_t tbuf_idx = 0;



static void NEC_Decoder(void);
static uint8_t decode_8t(uint8_t *t_in);
static uint16_t check_address(uint8_t *t_in);



/* 定时器2中断程序 ------------------------------------- */
void T2_IRQ() interrupt 12           //中断入口
{
	tout_flag = 1;
	AUXR &= ~0x10;
//  IE2 &= ~0x04;                   //若需要手动清除中断标志,可先关闭中断,此时系统会自动清除内部的中断标志
//  IE2 |= 0x04;                    //然后再开中断即可
}

void EXINT3_IRQ() interrupt 11          //INT3中断入口
{
	#define read_timing_100us()							(RELOAD_VALUE_MS*10 - (uint16_t)(CNT2US100((uint16_t)(T2H<<8) + T2L)))
	#define stop_timing()							(AUXR &= ~0x10)
	#define start_timing(timeout_ms)	do{\
																			tout_flag = 0;\
																			T2H = RELOADMS(timeout_ms)>>8;\
																			T2L = RELOADMS(timeout_ms);\
																			AUXR |= 0x10;\
																		}while(0)
	uint16_t t;
	EA=0;
	stop_timing();
	if(tout_flag == 0){
		t = read_timing_100us();
		if(tbuf_idx < sizeof(tbuf) && (IS_NEC_0(t) || IS_NEC_1(t) || IS_NEC_Pre(t))){
			tbuf[tbuf_idx++] = t;
			NEC_Decoder();
//			if(IS_NEC_0(t))
//				SBUF = '0';//NEC_Decoder();
//			else if(IS_NEC_1(t))
//				SBUF = '1';//NEC_Decoder();
//			else if(IS_NEC_Pre(t))
//				SBUF = 'P';//NEC_Decoder();
//			
		}
		else{
			tbuf_idx = 0;
		}
	}
	start_timing(RELOAD_VALUE_MS);
	EA=1;
//  INT_CLKO &= 0xDF;               //若需要手动清除中断标志,可先关闭中断,此时系统会自动清除内部的中断标志
//  INT_CLKO |= 0x20;               //然后再开中断即可
}


/* 把测量的码元时间，译成NEC码值 ---------------------------- */
static void NEC_Decoder(void){
#define Posi_Pre	0
#define Posi_Addr	1
#define Posi_Data	17
	uint8_t a;
	if(tbuf_idx < Posi_Data+16)
		goto __ignore;
	if(!IS_NEC_Pre(tbuf[Posi_Pre])){
		goto __fail;
	}

	/* 匹配地址码 ------------------------------- */
	if(check_address(tbuf+Posi_Addr) != IR_ADDR)
		goto __fail;

	/* 译码 ------------------------------------- */
	a = decode_8t(tbuf+Posi_Data);
	if(a != ERROR_VALUE){
		ir_key = a;
		SBUF = a;
		goto __success;
	}
	else
		goto __fail;
	
	
__ignore:
	return;
__success:
	tbuf_idx = 0;
	return;
__fail:
	tbuf_idx = 0;
	return;
}

static uint16_t check_address(uint8_t *t_in){
	uint16_t B = 0;
	uint8_t j;
	for(j=0;j<16;j++){
		if(IS_NEC_0(t_in[j])){
			
		}
		else if(IS_NEC_1(t_in[j])){
			B |= (uint16_t)1<<j; 
		}
		else{
			goto __fail;
		}
	}
__success:
	return B;
__fail:
	return ERROR_VALUE;
}


static uint8_t decode_8t(uint8_t *t_in){

	uint16_t B = 0;
	uint8_t j;
	for(j=0;j<16;j++){
		if(IS_NEC_0(t_in[j])){
			
		}
		else if(IS_NEC_1(t_in[j])){
			B |= (uint16_t)1<<j; 
		}
		else{
			goto __fail;
		}
	}
	j = B>>8;
	if(j+B&0x00ff == 0xff){
		goto __success;
	}
	else{
		goto __fail;
	}
__success:
	return B&0xff;
__fail:
	return ERROR_VALUE;
}


//http://www.51hei.com/bbs/dpj-81138-1.html

void ir_init(void)
{
	/* 配置定时器2 ----------------------------- */
//	#define T1MS (65536-Fosc/1000)      //1T模式
//	AUXR |= 0x04;                   //定时器2为1T模式
//	#define T1MS (65536-FOSC/12/1000) //12T模式
  AUXR &= ~0x04;                  //定时器2为12T模式
	AUXR &= ~0x10;	//关闭定时器2
	
//	T2L = RELOADMS(20);                     //初始化计时值
//	T2H = RELOADMS(20)>>8;
//	AUXR |= 0x10;                   //定时器2开始计时
	
	IE2 |= 0x04;                    //开定时器2中断
	//EA = 1;
	
	/* 配置外部中断3 --------------------------- */
  INT_CLKO |= 0x20;               //(EX3 = 1)使能INT3中断，下降沿触发
}

uint8_t Take_IR_Key(void){
	uint8_t t = ir_key;
	ir_key = 0;
	return t;
}

