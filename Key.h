#ifndef __KEY_H__
#define __KEY_H__

#include "c_types.h"


#define KEY_IDLE        1
#define KEY_ACTV        0

#define LONG_KEY        0x00010000
#define SHORT_KEY       0x00000001

#define NO_KEY          0
#define KEY_POLL_INTV		10

/************ application specific macro remaps *******************************/

enum KeyId {KeyID_PwrSaving=0,KeyId_Pwr=1,KeyId_AntiShock=2,KeyId_Menu,KeyId_Inc,KeyId_Dec,KeyId_Confirm,KeyId_ESC,KeyId_SwBuzzer,KeyId_SwTiming,KeyId_SwScreen,KeyId_ClrTiming};


#define KEY_MSK_PwrSaving		(SHORT_KEY<<KeyID_PwrSaving)
#define KEY_MSK_Pwr					(SHORT_KEY<<KeyId_Pwr)
#define KEY_MSK_AntiShock		(SHORT_KEY<<KeyId_AntiShock)
#define NbrOfhwSwitch				3
#define KEY_MSK_Menu				(1UL<<KeyId_Menu)
#define KEY_MSK_Inc					(1UL<<KeyId_Inc)
#define KEY_MSK_Dec					(1UL<<KeyId_Dec)
#define KEY_MSK_Confirm			(1UL<<KeyId_Confirm)
#define KEY_MSK_ESC					(1UL<<KeyId_ESC)
#define KEY_MSK_SwBuzzer		(1UL<<KeyId_SwBuzzer)
#define KEY_MSK_SwTiming		(1UL<<KeyId_SwTiming)
#define KEY_MSK_SwScreen		(1UL<<KeyId_SwScreen)
#define KEY_MSK_ClrTiming		(1UL<<KeyId_ClrTiming)

/* Exported functions ------------------------------------------------------- */
void Key_Init(void);
void ScanKey( void );
uint32_t TakeKey(void);
#endif 
