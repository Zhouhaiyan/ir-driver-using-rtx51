#ifndef __GSM_COMMON_H__
#define __GSM_COMMON_H__

#include <STC15F2K60S2.H>
#include "user_config.h"

//port to OS
#include "os_api.h"


#define os_sleep_ms(X)			OS_DELAY(X)

extern unsigned char xdata num_bound[24];
/*
		memset(send_num,'\0',12);
		strcat((char*)send_num,(char*)num_bound); //�Ѱ󶨺��븴�Ƶ�Ҫ���͵ĺ���Ĵ�����
*/
extern unsigned char xdata send_num[24];

extern unsigned char xdata msg_response_buf[400];

void clr_buf(void);
void sim900a_sms(char *num,char *smstext);
bit get_number_from_ucs(unsigned char len);

void OS_DELAY(unsigned int);

#endif