
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APL_H__
#define __APL_H__

#include "c_types.h"

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
enum EVTx{\
	EVT_GSM_Bind = 0,\
	EVT_Power,\
	EVT_SwScreen,\
	EVT_SwTiming,\
	EVT_GSM_Inquiry,\
	EVT_CFG1,\
	EVT_CFG2,\
	EVT_CFG3,\
	EVT_CFG4,\
	EVT_CFG5,\
	EVT_CFG6,\
	EVT_CFG7,\
	EVT_CFG8,\
	EVT_CFG9,\
	EVT_GSM_Unbind,\
	EVT_Temperature,\
	EVT_BatteryVolt,\
	EVT_HighVolt,\
	EVT_ShockCounter,\
	EVT_Timing,\
	EVT_SwBuzzer,\
	EVT_ClrTiming,\
	EVT_Shock,\
	EVT_TouchLine,\
	EVT_OverCurrent,\
	EVT_BatteryLow,\
	EVT_OverHeat,\
	EVT_AntiShock,\
	EVT_Beep,\
	EVT_MAX,\
};


#define EVT_MASK_GSM_Bind			(1UL<<EVT_GSM_Bind)
#define EVT_MASK_Power				(1UL<<EVT_Power)
#define EVT_MASK_SwScreen			(1UL<<EVT_SwScreen)
#define EVT_MASK_SwTiming			(1UL<<EVT_SwTiming)
#define EVT_MASK_GSM_Inquiry	(1UL<<EVT_GSM_Inquiry)
#define EVT_MASK_CFG1					(1UL<<EVT_CFG1)
#define EVT_MASK_CFG2					(1UL<<EVT_CFG2)
#define EVT_MASK_CFG3					(1UL<<EVT_CFG3)
#define EVT_MASK_CFG4					(1UL<<EVT_CFG4)
#define EVT_MASK_CFG5					(1UL<<EVT_CFG5)
#define EVT_MASK_CFG6					(1UL<<EVT_CFG6)
#define EVT_MASK_CFG7					(1UL<<EVT_CFG7)
#define EVT_MASK_CFG8					(1UL<<EVT_CFG8)
#define EVT_MASK_CFG9					(1UL<<EVT_CFG9)
#define EVT_MASK_GSM_Unbind		(1UL<<EVT_GSM_Unbind)
#define EVT_MASK_Temperature	(1UL<<EVT_Temperature)
#define EVT_MASK_BatteryVolt	(1UL<<EVT_BatteryVolt)
#define EVT_MASK_HighVolt			(1UL<<EVT_HighVolt)
#define EVT_MASK_ShockCounter	(1UL<<EVT_ShockCounter)
#define EVT_MASK_Timing				(1UL<<EVT_Timing)
#define EVT_MASK_SwBuzzer			(1UL<<EVT_SwBuzzer)
#define EVT_MASK_ClrTiming		(1UL<<EVT_ClrTiming)
#define EVT_MASK_Shock				(1UL<<EVT_Shock)
#define EVT_MASK_TouchLine		(1UL<<EVT_TouchLine)
#define EVT_MASK_OverCurrent	(1UL<<EVT_OverCurrent)
#define EVT_MASK_BatteryLow		(1UL<<EVT_BatteryLow)
#define EVT_MASK_OverHeat			(1UL<<EVT_OverHeat)
#define EVT_MASK_AntiShock		(1UL<<EVT_AntiShock)
#define EVT_MASK_Beep					(1UL<<EVT_Beep)






#define GSMView						0x01
#define Screen1View				0x02
#define Screen2View				0x04
#define Screen3View				0x08
#define Screen4View				0x10

/* Exported macro ------------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/
typedef enum {OPC_NOP,OPC_TGL,OPC_INC,OPC_DEC,OPC_SET,OPC_RESET,OPC_ASSIGN,OPC_ACK,OPC_NOTIFY} eOPC_t;
typedef enum {PWR_OFF,PWR_ON,PWR_SAVING,PWR_VALIDE} ePWR_t;
typedef uint32_t Event_t;

typedef struct{
	uint32_t value;
	uint32_t step;
	uint32_t min;
	uint32_t max;
	uint32_t unit;
}config_u32_t;

typedef struct{
	uint16_t value;
	uint16_t step;
	uint16_t min;
	uint16_t max;
	uint16_t unit;
}config_u16_t;

typedef struct{
	uint8_t value;
	uint8_t step;
	uint8_t min;
	uint8_t max;
	uint8_t unit;
}config_u8_t;

typedef struct{
	
	//input
	int8_t 		temperature;
	uint16_t 	battery_voltage_100mv;
	uint32_t 	high_voltage;
	
	
	//core logic, highest priority
	__O uint8_t		inveter_switch;
	__I uint8_t 	inveter_current;
	
	__O uint8_t		anti_shock_relay_output;
	__I uint8_t		anti_shock_input;
	
	
	
	//behavior
	uint8_t 	power;//0-Power Off;1-Normal Power On;2-Periodical Power On.
	
	uint8_t 	shock_counter;
	uint32_t	shock_time_ms;
	uint8_t		shock_energy;
	uint8_t 	touch_line_flag;
	
	
	uint8_t 	battery_low_flag;//event
	uint16_t 	timing_on;
	uint16_t	timing_off;
	
	//behavior configs
	union{
		struct{
			config_u16_t	se1_high_voltage;
			config_u16_t	se2_shock_current;
			config_u16_t	se3_touch_line_protect_time;//unit: senconds
			config_u16_t	se4_periodical_on_time;
			config_u16_t	se5_periodical_off_time;
			config_u16_t	se6_timing_on;//every 30minutes
			config_u16_t	se7_timing_off;//every 30minutes
			config_u16_t	se8_battery_low_voltage_100mv;//8v~100v
			config_u16_t	se9_over_current;//10:10:990, default 400
		}stconfigs;
		config_u16_t se_array[9];
	}unconfigs;
	
	//miscellaneous
	uint8_t screen_switch;
	uint8_t timing_switch;
	uint8_t	buzzer_switch;
	uint8_t anti_shock_sw;
	uint8_t fan_switch;
	
	//GSM Bind Info
	uint8_t bind_status;
	uint8_t bind_tels[3][20];
	
	
}Model_t;

typedef struct{
	Event_t msgGSM;//通过GSM呈现
	Event_t msgScreen1;//常规显示
	Event_t msgScreen2;//显示温度
	Event_t msgScreen3;//显示定时
	Event_t msgScreen4;//显示设置
	Event_t msg;//默认消息，处理LED指示灯、控制数码管显示。
	uint8_t CurView;
}View_t;

typedef struct{
	Event_t msg;
}Control_t;


typedef struct{
	Model_t m;
	View_t v;
	Control_t c;
	uint8_t *msg_buf[EVT_MAX];//@todo:未分配内存
}App_t;
extern xdata App_t app;

extern xdata volatile uint32_t SysTime;//单位是毫秒

extern xdata volatile uint32_t SysSecond;//单位是秒
/* Exported functions ------------------------------------------------------- */

void SAVE_NV_PARAM(void);
void LOAD_NV_PARAM(void);

#ifdef __cplusplus
}
#endif 


#endif