#include "utest.h"
#include <STC15F2K60S2.H>
#include "TM1640.h"
#include "TIMER.H"
#include "uart.h"
#include "EEPROM.H"

#include "SIM900A.H"

#define TEST_UART		0
#define TEST_TM1640	1
#define TEST_EEPROM	0
#define TEST_GSM		0
void test_board(void){
	/*���Դ������*/
	#if TEST_UART
	unsigned int a = 1000;
	EA = 0;
	Timer0_Init();
	Uart_Init();
	EA = 1;
	while(1){
		Uart_SendByte('1');
		Uart_SendByte('2');
		Uart_SendByte('3');
		Uart_SendByte('4');
		while(a--);
		a = 60000;
	}
	#endif
	
	#if TEST_TM1640
	/*����TM1640*/
	TM1640_Init(DSP1640_ENB);
	#endif
	
	#if TEST_EEPROM
	EEPROM_TEST();
	#endif
	
	#if TEST_GSM
	test_gsm();
	#endif
	while(1);
}