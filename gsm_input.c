#include "string.h"
#include "stdio.h"
#include "gsm_utils.h"
#include "gsm_common.h"
#include "gsm_port.h"

#define EXCUTE_CMD	0
/******************************************************************************************
** 函数名称 ： set_num_pr()函数
** 函数功能 ： 提取绑定号码
** 入口参数 ： len 处理内容长度
** 出口参数 ： num_bound 绑定号码
** 作者：gsl
** 修改记录：
******************************************************************************************/	
bit set_num_pr(unsigned char len)
{
	unsigned char xdata i,j,ch=0;
	unsigned char xdata temp[50];
	memset(temp,'\0',50);	
	for(i=len;i<48+len;i++)
	{
		if(!msg_recv_buf[i]) 
		{
			temp[i-len]='\0';
			break;
		}
		else 
		temp[i-len]=msg_recv_buf[i];
	}
	if(temp[0]=='0'&&temp[1]=='0'&&temp[2]=='3')//数字开头的GBK编码
	{
		for(j=0;j<((i-len)/4);j++)
		{
			num_bound[j]=(temp[4*j+3]);	
		}	
		return 1;
	}
	else
		return 0;
}


static unsigned int xdata number_from_ucs=0;
/******************************************************************************************
** 函数名称 ： get_number_from_ucs()函数
** 函数功能 ： 接收到的参数进行处理  
** 入口参数 ： offset 处理内容长度
** 出口参数 ： number_from_ucs 提取短信命令内容
** 作者：gsl
** 修改记录：
******************************************************************************************/	
bit get_number_from_ucs(unsigned char offset)
{
	unsigned char xdata i,j,ch=0;
	unsigned char xdata temp[22];
	unsigned char xdata data_temp[10];
  unsigned char xdata dot_flag=0;	
	memset(temp,'\0',sizeof(temp));	
	for(i=offset;i<20+offset;i++)
	{
		if(!msg_recv_buf[i]) 
		{
			temp[i-offset]='\0';
			break;
		}
		else 
		temp[i-offset]=msg_recv_buf[i];
	}	
	if(temp[0]=='0'&&temp[1]=='0'&&temp[2]=='3')//数字开头的GBK编码
	{
		memset(data_temp,'\0',10);		
		for(j=0;j<((i-offset)/4);j++)
		{
			if(temp[4*j+3]!='E' && temp[4*j+2]!='2')//"002E"是"."的GBK码
				data_temp[j]=(temp[4*j+3]);	
			else 
				dot_flag=1;	
		}		
		number_from_ucs=0;
	if(!dot_flag)
	{
		for(i=0;i<10;i++)
			{
				ch = data_temp[i];
				if (ch < '0' || ch > '9')
					break;
				number_from_ucs = number_from_ucs*10 + (ch - '0');
			}
	}
	else if(data_temp[0] >= '0' && data_temp[0] <= '9')
	{
	// 	number_from_ucs=12345;
			number_from_ucs=((data_temp[0]-'0')*10+(data_temp[2]-'0'));	
			dot_flag=0;
	}
		
		return 1;
	}
	else
		return 0;
}

/******************************************************************************************
** 函数名称 ： Task_Rec_Sms()函数
** 函数功能 ：读取gsm接收的短信内容，并把发送的号码和短信内容提取出来
** 入口参数 ： Rec_Sms_Fg 接收短信标志 1 接收到 0 未接收到
** 出口参数 ： Sms_Hf_Fg 回复标志 1 需要回复 0 不需要回复
** 修改记录：
******************************************************************************************/	
void Task_Rec_Sms(void)
{ 
	static unsigned char xdata read_msg_cmd_buf[12];
	static unsigned char xdata rec_sms_num[24];//接收号码缓冲区
	static unsigned char xdata msgindex[3];
	unsigned char xdata *p;
	unsigned char xdata i;	
	unsigned char xdata *str;
	
	os_sleep_ms(500);//???
	memset(msgindex,'\0',sizeof(msgindex));	
	memset(read_msg_cmd_buf,'\0',sizeof(read_msg_cmd_buf));
	memset(rec_sms_num,'\0',sizeof(rec_sms_num));
	memset(msg_recv_buf,'\0',sizeof(msg_recv_buf));		

	/* 1. 解析新短信索引到msgindex ---------------- */
	str = strstr((char*)msg_recv_buf,",") +1;	/* 查找新短信索引所在位置 */
	for(i=0;i<5;i++){
		if(str[i]==0x0d && str[i+1]==0x0a){//接收到换行时结束解析。
			msgindex[i]='\0';
			break;
		}
		else if(str[i]>'9'||str[i]<'0'){//遇到非'0'~'9'时退出解析。
			goto __fail;
		}
		else 
			msgindex[i]=str[i];
	}

	/* 2. 读新来短信到msg_recv_buf ---------------------------- */
	sprintf((char*)read_msg_cmd_buf,"AT+CMGR=%s\r",msgindex);//读短信
	if(AGSM_OK != gsm_agent(read_msg_cmd_buf,"OK"))
		goto __fail;
	
	/* 3. 提取短信号码到rec_sms_num ---------------------- */
	p=strstr((char*)msg_recv_buf,",");  
	p=strtok(p,",");//分割字符串	
	for(i=0;i<11;i++)
	{
		rec_sms_num[i]=p[1+i];
	}
	rec_sms_num[i]='\0';
	
	/* 4.1 如果号码没有绑定过，删除此短信并退出 -------------*/
	if(!hand1(rec_sms_num,num_bound))	
	{
		GSM_WriteString("AT+CMGD=1,4\r"); //发送AT+CSCS="UCS2"，等待回复OK		
		goto __auth_fail;
	}
	
	/* 4.2 如果号码已绑定，解析指令到msg_recv_buf[] -------------*/
	p=strtok(NULL,",");
	p=strtok(NULL,"\"\"");
	p=strtok(NULL,"\r\n");
	for(i=0;i<sizeof(msg_recv_buf);i++){
		if(p[i]==0x0d && p[i+1]==0x0a){
			msg_recv_buf[i]='\0';
			break;
		}
		else{
			msg_recv_buf[i]=p[i];//把接收的内容放入msg_recv_buf[]数组中
		}
	}
	
	/* 5. 执行指令 ------------------------ */
	memset(msg_response_buf,'\0',400);//初始化发送缓冲区
#if EXCUTE_CMD		
	if(hand1(msg_recv_buf,str_stop)&&get_number_from_ucs(8)) 	//停机电压设置								
	{
		stop_vol=number_from_ucs;
		cs_save_pr();
		strcat((char*)msg_response_buf,(char*)str_stop); 
		strcat((char*)msg_response_buf,(char*)str_dy_hf); 
		strcat((char*)msg_response_buf,(char*)str_set_hf); 
		strcat((char*)msg_response_buf,(char*)str_fdrbh_succes); 	
		strcat((char*)msg_response_buf,(char*)str_khl); 
		strcat((char*)msg_response_buf,(char*)msg_recv_buf+8);	
		strcat((char*)msg_response_buf,(char*)str_vol); 			
	}
	else if(hand1(msg_recv_buf,str_bd)&&set_num_pr(8)) 	//绑定号码								
	{
					num_save_pr();
					strcat((char*)msg_response_buf,(char*)str_bd); 
					strcat((char*)msg_response_buf,(char*)str_fdrbh_succes); 	
					strcat((char*)msg_response_buf,(char*)str_khl); 
					strcat((char*)msg_response_buf,(char*)msg_recv_buf+8);	
					strcat((char*)msg_response_buf,(char*)str_khr);
					num_bd_flag=1;						
	}	
	else  if(hand1(msg_recv_buf,str_jb_hf)&&(strlen(msg_recv_buf)==8)) 	//解绑号码								
	{
					strcat((char*)msg_response_buf,(char*)str_jb_hf);
					strcat((char*)msg_response_buf,(char*)str_fdrbh_succes); 	
					num_jb_flag=1;
	}
	else if(hand1(msg_recv_buf,str_kd)&&get_number_from_ucs(8)) 	//电瓶匮电设置									
	{
			kd_vol=number_from_ucs;	
			cs_save_pr();
			strcat((char*)msg_response_buf,(char*)str_dp_hf); //电瓶
			strcat((char*)msg_response_buf,(char*)str_kd); //匮电
			strcat((char*)msg_response_buf,(char*)str_bh); //保护
			strcat((char*)msg_response_buf,(char*)str_dy_hf); //电压		
			strcat((char*)msg_response_buf,(char*)str_set_hf); //设置
			strcat((char*)msg_response_buf,(char*)str_fdrbh_succes); //成功			
			strcat((char*)msg_response_buf,(char*)str_khl); 
			strcat((char*)msg_response_buf,(char*)msg_recv_buf+8);	
			strcat((char*)msg_response_buf,(char*)str_vol); 
	}	
	else if(hand1(msg_recv_buf,str_on)&&hand1(msg_recv_buf,str_ji)&&(strlen(msg_recv_buf)==8)) 	//开机						
	{
			//@todo 执行开机逻辑
			strcat((char*)msg_response_buf,(char*)str_on_hf);  //发送提醒您—短信开机成功
	}
	else if(hand1(msg_recv_buf,str_off)&&hand1(msg_recv_buf,str_ji)&&(strlen(msg_recv_buf)==8)) 	//关机						
	{
			strcat((char*)msg_response_buf,(char*)str_off_hf);    //发送提您—短信
			strcat((char*)msg_response_buf,(char*)str_off);  //关
			strcat((char*)msg_response_buf,(char*)str_ji);  //机
			strcat((char*)msg_response_buf,(char*)str_fdrbh_succes);  //成功
	}	
	else if(hand1(msg_recv_buf,str_kj)&&hand1(msg_recv_buf,str_fdrgn)&&(strlen(msg_recv_buf)==28)) 	//开启防电人模式					
	{
				fdr_flag=1;
				safe_led=0;
	// 			cs_save_pr();
				strcat((char*)msg_response_buf,(char*)str_kj);    //开启
				strcat((char*)msg_response_buf,(char*)str_fdrmode);  //防电人模式			
				strcat((char*)msg_response_buf,(char*)str_fdrbh_succes);  //成功
				strcat((char*)msg_response_buf,(char*)str_gth);  //!
	}
	else if(hand1(msg_recv_buf,str_gb)&&hand1(msg_recv_buf,str_fdrgn)&&(strlen(msg_recv_buf)==28)) 	//关闭防电人模式					
	{
				fdr_flag=0;
				safe_led=1;
	// 		  cs_save_pr();
				strcat((char*)msg_response_buf,(char*)str_gb);    //关闭
				strcat((char*)msg_response_buf,(char*)str_fdrmode);  //防电人模式			
				strcat((char*)msg_response_buf,(char*)str_fdrbh_succes);  //成功
				strcat((char*)msg_response_buf,(char*)str_gth);  //!
	}			
	else if(hand1(msg_recv_buf,str_ych)&&get_number_from_ucs(8)) 	//压差设置		
	{	
			ych_val=number_from_ucs;
			cs_save_pr();
			strcat((char*)msg_response_buf,(char*)str_ych); //压差
			strcat((char*)msg_response_buf,(char*)str_dy_hf); //电压		
			strcat((char*)msg_response_buf,(char*)str_set_hf); //设置
			strcat((char*)msg_response_buf,(char*)str_fdrbh_succes); //成功			
			strcat((char*)msg_response_buf,(char*)str_khl); 
			strcat((char*)msg_response_buf,(char*)msg_recv_buf+8);	
			strcat((char*)msg_response_buf,(char*)str_vol);
	}
	else if(hand1(msg_recv_buf,str_bj)&&get_number_from_ucs(8)) 	//报警电压设置		
	{
			bj_vol=number_from_ucs;
			cs_save_pr(); 
			strcat((char*)msg_response_buf,(char*)str_bj); //压差
			strcat((char*)msg_response_buf,(char*)str_dy_hf); //电压		
			strcat((char*)msg_response_buf,(char*)str_set_hf); //设置
			strcat((char*)msg_response_buf,(char*)str_fdrbh_succes); //成功			
			strcat((char*)msg_response_buf,(char*)str_khl); 
			strcat((char*)msg_response_buf,(char*)msg_recv_buf+8);	
			strcat((char*)msg_response_buf,(char*)str_vol);
	}
	else if(hand1(msg_recv_buf,str_cs)&&(strlen(msg_recv_buf)==8)) 	//参数
	{
			strcat((char*)msg_response_buf,(char*)str_gy_hf); //高压
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			if(stop_vol<10000)
				hex_c_ucs(stop_vol,4);
			else 
				hex_c_ucs(stop_vol,5);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //高压值
			strcat((char*)msg_response_buf,(char*)str_v); //高压值			
			strcat((char*)msg_response_buf,(char*)str_enter); //换行	

			strcat((char*)msg_response_buf,(char*)str_bj); //报警
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			if(bj_vol<10000)
				hex_c_ucs(bj_vol,4);
			else 
				hex_c_ucs(bj_vol,5);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //报警压值
			strcat((char*)msg_response_buf,(char*)str_v); //报警压值			
			strcat((char*)msg_response_buf,(char*)str_enter); //换行
			
			strcat((char*)msg_response_buf,(char*)str_ych); //压差
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			if(ych_val<10000)
				hex_c_ucs(ych_val,4);
			else 
				hex_c_ucs(ych_val,5);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //报警压值
			strcat((char*)msg_response_buf,(char*)str_v); //报警压值			
			strcat((char*)msg_response_buf,(char*)str_enter); //换行
			
			strcat((char*)msg_response_buf,(char*)str_kd); //馈电
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			hex_c_ucs(kd_vol/10.0,4);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //馈电值
			strcat((char*)msg_response_buf,(char*)str_v); //馈电值	
			strcat((char*)msg_response_buf,(char*)str_enter); //换行

			strcat((char*)msg_response_buf,(char*)str_fm_hf); //蜂鸣
			strcat((char*)msg_response_buf,(char*)str_mh); //：			
			if(!bj_off_flag)
			{
				strcat((char*)msg_response_buf,(char*)str_on); //开	
			}
			else
			{
				strcat((char*)msg_response_buf,(char*)str_off); //关
			}
			strcat((char*)msg_response_buf,(char*)str_enter); //换行
			
      strcat((char*)msg_response_buf,(char*)str_yx); //压线
			strcat((char*)msg_response_buf,(char*)str_mh); //：		
			strcat((char*)msg_response_buf,(char*)str_300S_hf); //300S		
			
			strcat((char*)msg_response_buf,(char*)str_enter); //换行	

      strcat((char*)msg_response_buf,(char*)str_wl_hf); //网络
			strcat((char*)msg_response_buf,(char*)str_mh); //：		
			strcat((char*)msg_response_buf,(char*)str_4G_hf); //4G		
			strcat((char*)msg_response_buf,(char*)str_enter); //换行				
					
			strcat((char*)msg_response_buf,(char*)str_fdrgn); //防电人功能
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			if(fdr_flag)
				strcat((char*)msg_response_buf,(char*)str_kj); //开启	
      else
				strcat((char*)msg_response_buf,(char*)str_gb); //关闭
			strcat((char*)msg_response_buf,(char*)str_enter); //换行	
			
		
			strcat((char*)msg_response_buf,(char*)str_wxdw_hf); //卫星定位
			strcat((char*)msg_response_buf,(char*)str_mh); //：				
			strcat((char*)msg_response_buf,(char*)str_on); //开
			strcat((char*)msg_response_buf,(char*)str_enter); //换行
			
			strcat((char*)msg_response_buf,(char*)str_bbh_hf); //版本：V9
			strcat((char*)msg_response_buf,(char*)str_enter); //换行		
	}
	else if(hand1(msg_recv_buf,str_cx)&&(strlen(msg_recv_buf)==8)) 	//查询	
	{
			strcat((char*)msg_response_buf,(char*)str_gy_hf); //高压
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			if(high_vol<10000)
				hex_c_ucs(high_vol,4);
			else 
				hex_c_ucs(high_vol,5);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //高压值
			strcat((char*)msg_response_buf,(char*)str_v); //高压值			
			strcat((char*)msg_response_buf,(char*)str_enter); //换行						
			strcat((char*)msg_response_buf,(char*)str_dp_hf); //电瓶
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			hex_c_ucs(vol_temp/10.0,4);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //电瓶值
			strcat((char*)msg_response_buf,(char*)str_v); //高压值	
			strcat((char*)msg_response_buf,(char*)str_enter); //换行
			
			strcat((char*)msg_response_buf,(char*)str_fd_hf); //放电
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			hex_c_ucs(chfa_num,3);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //放电次数
			strcat((char*)msg_response_buf,(char*)str_ci_hf); //次	
			strcat((char*)msg_response_buf,(char*)str_enter); //换行	
			strcat((char*)msg_response_buf,(char*)str_fdrgn); //防电人功能
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			if(fdr_flag)
			strcat((char*)msg_response_buf,(char*)str_kj); //开启	
      else
			strcat((char*)msg_response_buf,(char*)str_gb); //关闭
			strcat((char*)msg_response_buf,(char*)str_enter); //换行	
			
			strcat((char*)msg_response_buf,(char*)str_temp); //温度
			strcat((char*)msg_response_buf,(char*)str_mh); //：
			hex_c_ucs(temp_f_vol,4);
			strcat((char*)msg_response_buf,(char*)msg_recv_buf); //温度值
			strcat((char*)msg_response_buf,(char*)str_tempfu); //高压值	
		  strcat((char*)msg_response_buf,(char*)str_enter); //换行

			strcat((char*)msg_response_buf,(char*)str_zht); //状态
			strcat((char*)msg_response_buf,(char*)str_mh); //：			
			if(on_flag)
			{
				strcat((char*)msg_response_buf,(char*)str_on); //开
				strcat((char*)msg_response_buf,(char*)str_ji); //机		
			}
			else if(off_flag)
			{
				strcat((char*)msg_response_buf,(char*)str_off); //关
				strcat((char*)msg_response_buf,(char*)str_ji); //机		
			}
			strcat((char*)msg_response_buf,(char*)str_enter); //换行

			strcat((char*)msg_response_buf,(char*)str_yx); //压线
			strcat((char*)msg_response_buf,(char*)str_mh); //：			
			if(yx_flag)
			{
				strcat((char*)msg_response_buf,(char*)str_yes); //是	
			}
			else
			{
				strcat((char*)msg_response_buf,(char*)str_no); //否
			}
			strcat((char*)msg_response_buf,(char*)str_enter); //换行			

			strcat((char*)msg_response_buf,(char*)str_kd); //匮电
			strcat((char*)msg_response_buf,(char*)str_mh); //：			
			if(kd_flag)
			{
				strcat((char*)msg_response_buf,(char*)str_yes); //是	
			}
			else
			{
				strcat((char*)msg_response_buf,(char*)str_no); //否
			}
			strcat((char*)msg_response_buf,(char*)str_enter); //换行		
	
			strcat((char*)msg_response_buf,(char*)str_xh); //信号
			strcat((char*)msg_response_buf,(char*)str_mh); //：			
			if(xhjd<5)
			{
				strcat((char*)msg_response_buf,(char*)str_r); //弱
			}
			else if(xhjd>=5&&xhjd<10)
			{
				strcat((char*)msg_response_buf,(char*)str_z); //中
			}
			else
			{
				strcat((char*)msg_response_buf,(char*)str_q); //否
			}
			strcat((char*)msg_response_buf,(char*)str_enter); //换行					
		
			strcat((char*)msg_response_buf,(char*)str_dm); //代码
			strcat((char*)msg_response_buf,(char*)str_mh); //：			
			if(err_code==0)
			{
				strcat((char*)msg_response_buf,(char*)str_zch); //正常
			}
			
			else if(err_code==0)
			{
				strcat((char*)msg_response_buf,(char*)str_z); //中
			}
			else
			{
				strcat((char*)msg_response_buf,(char*)str_ehg); //E--
				hex_c_ucs(err_code,1);
				strcat((char*)msg_response_buf,(char*)msg_recv_buf); //错误码代
				strcat((char*)msg_response_buf,(char*)str_enter); //换行	
		  }
	}
	else
	{
		strcat((char*)msg_response_buf,(char*)str_fail);  //操作失败或指令有误
	}
	#endif
__auth_fail:
__fail:
	return;
}		
