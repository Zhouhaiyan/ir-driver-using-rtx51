#ifndef __C_TYPES_H__
#define __C_TYPES_H__


#ifndef uint8_t
typedef unsigned char uint8_t;
#endif
	 
#ifndef int8_t
typedef signed char int8_t;
#endif
	 
#ifndef uint16_t
typedef unsigned int uint16_t;
#endif
	 
#ifndef uint32_t
typedef unsigned long uint32_t;
#endif


#define uchar unsigned char
#define uint unsigned int 
#define ulong unsigned long 
	

#ifndef __I
#define __I	volatile
#endif

#ifndef __O	
#define __O
#endif

#endif