#include"sm1616.h"
unsigned  char xdata Smg_Disp_Two=0;//第二组数码管显示指针
unsigned  char xdata Smg_Disp_Four=0;//第四组数码管显示指针";
unsigned char code duanma[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x77,0x7c,
                               0x39,0x5e,0x79,0x71,0x40,0x00,0xbf,0x86,0xdb,0xcf,0xe6,0xed,
                               0xfd,0x87,0xff,0xef,0x38,0x76};
unsigned char xdata sm1616_data[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned int xdata t_test=0;

extern  bit Tmr_20ms_Fg;
extern unsigned int xdata vol_adj;
extern unsigned int  xdata vol_temp;//电瓶电压值
extern unsigned long xdata high_vol;
extern unsigned int xdata stop_vol;//停机电压值
extern unsigned int xdata bj_vol;
extern unsigned int xdata ych_val;
extern unsigned char xdata chfa_num;
extern unsigned char xdata Hf_Code;
extern unsigned char   Degree_C,Degree_0C;//温度值
extern unsigned char xdata err_code;
extern unsigned char xdata set_point;
extern unsigned char xdata kd_vol;//匮电值
extern unsigned long xdata timeingon_temp;//定时开机设置寄存器
extern unsigned long xdata timeingoff_temp;//定时关机设置寄存器
void disp_num_pro(unsigned int num);
	//共阴数码管段码数据(0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,-, ,0.,1.,2.3.4.5.6.7.8.9.,L,H),
	
/*********************************************************
功能：启动I2C信号
*********************************************************/
void SM1616_Start(void)
{
	SM1616_SDA_SET;
	SM1616_SCL_SET;
	DELAY;
	SM1616_SDA_CLR;
	SM1616_SCL_CLR;
}
/*********************************************************
功能：SM1616应答信号
*********************************************************/
void SM1616_Ack(void)
{
	DELAY;
	SM1616_SCL_CLR;
	DELAY;
	SM1616_SCL_SET;
	DELAY;
	SM1616_SCL_CLR;
}
/*********************************************************
功能：停止I2C信号
*********************************************************/
void SM1616_Stop(void)
{
	SM1616_SDA_CLR;
	SM1616_SCL_SET;
	DELAY;
	SM1616_SDA_SET;
}
/*********************************************************
功能：向SM1616发送8位数据，从高位开始
*********************************************************/
void SM1616_Send_8bit(uchar dat)
{
	uchar i;
	for(i=0;i<8;i++)
	{
		SM1616_SCL_CLR;
		if(dat&0x80)   //从高位开始
		{
		  SM1616_SDA_SET;
		}
		else
		{
	      SM1616_SDA_CLR;
		}
		DELAY;
		dat=dat<<1;
		SM1616_SCL_SET;
		DELAY;
	}
	SM1616_Ack();
}
void Display_pr(uchar *p)//全显
{
 	uchar i;
	SM1616_Start();
	SM1616_Send_8bit(SM1616_DIG0);       //加载数据，从00H开始
	for(i=0;i<3;i++)	   //送16字节显示数据，采用地址自动加1模式
	{
		SM1616_Send_8bit(duanma[p[i]]);   //全写1
	}	
	for(i=6;i>2;i--)	   //送16字节显示数据，采用地址自动加1模式
	{
		SM1616_Send_8bit(duanma[p[i]]);   //全写1
	}	
	
	for(i=9;i>6;i--)	   //送16字节显示数据，采用地址自动加1模式
	{
		SM1616_Send_8bit(duanma[p[i]]);   //全写1
	}		
	SM1616_Send_8bit(0x00);   //全写1
		for(i=10;i<15;i++)	   //送16字节显示数据，采用地址自动加1模式
	{
		SM1616_Send_8bit(duanma[p[i]]);   //全写1
	}	
	SM1616_Stop();
	SM1616_Start();
	SM1616_Send_8bit(SM1616_DIS_CONTROL);        //开显示（第一字节）
	SM1616_Send_8bit(SM1616_BIT_INTENS8);        //开显示（第二字节）
	SM1616_Stop();
}
/******************************************************************************************
** 函数名称 ：disp_data_pr()函数
** 函数功能 ：显示数据处理
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/
void disp_init_pr(void)
{
	sm1616_data[7]=0;
	sm1616_data[8]=0;
	sm1616_data[9]=0;
	sm1616_data[0]=0;
	sm1616_data[1]=0;
	sm1616_data[2]=0;	
  sm1616_data[3]=0;
	sm1616_data[4]=0;
	sm1616_data[5]=0;
	sm1616_data[6]=0;	
	sm1616_data[10]=0;
	sm1616_data[11]=0;
	sm1616_data[12]=0;	
	sm1616_data[13]=0;
	sm1616_data[14]=0;	
	Smg_Disp_Four=7;
}
/******************************************************************************************
** 函数名称 ：disp_data_pr()函数
** 函数功能 ：显示数据处理
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/
void disp_data_pr(void)
{	
	sm1616_data[0]=vol_temp/100;
	sm1616_data[1]=vol_temp/10%10+18;
	sm1616_data[2]=vol_temp%10;   //显示电瓶电压
	//显示触发次数
if(chfa_num>=100)	
{
	sm1616_data[7]=chfa_num/100;
	sm1616_data[8]=chfa_num/10%10;
	sm1616_data[9]=chfa_num%10;
}
else if(chfa_num>=10)
	{
	sm1616_data[7]=17;
	sm1616_data[8]=chfa_num/10%10;
	sm1616_data[9]=chfa_num%10;
}
else
	{
	sm1616_data[7]=17;
	sm1616_data[8]=17;
	sm1616_data[9]=chfa_num%10;
 }

	switch (Smg_Disp_Two) //第二组数码管显示内容
	{
		case 0:                                  //显示温度
				if(Degree_C<100)sm1616_data[3]=17;
			  else sm1616_data[3]=Degree_C/100%10;
				 sm1616_data[4]=Degree_C/10%10;
				 sm1616_data[5]=Degree_C%10+18;
				 sm1616_data[6]=Degree_0C%10;	
			break;
		case 1:                                //显示E--X
			 sm1616_data[3]=14;
			 sm1616_data[4]=16;
			 sm1616_data[5]=16;	
			 sm1616_data[6]=err_code%10;
			break;
		case 2:                                //显示LOCK
			 sm1616_data[3]=28;
			 sm1616_data[4]=0;
			 sm1616_data[5]=12;
			 sm1616_data[6]=29;//显示lock
			break;
		case 3:                                //显示SET
			sm1616_data[3]=5; 
			sm1616_data[4]=14;
			sm1616_data[5]=set_point+1;
			sm1616_data[6]=16;	
			break;
		case 4:                                //显示0ff0
			sm1616_data[3]=0; 
			sm1616_data[4]=16;
			sm1616_data[5]=16;
			sm1616_data[6]=0;	
			break;		
		default:
			break;
	}
	
	switch (Smg_Disp_Four)//第四组数码管显示内容
	{
		case 0:                      //显示放电，显示-Fd--
				sm1616_data[10]=16;
				sm1616_data[11]=15;
				sm1616_data[12]=13;	
				sm1616_data[13]=16;
				sm1616_data[14]=16;
			break;
		case 1: //显示停止电压
			disp_num_pro(stop_vol);
			break;
		case 2: //显示停止压差
			disp_num_pro(ych_val); 						
			break;
		case 3: //显示匮电值
			sm1616_data[10]=17;
			sm1616_data[11]=17;
			sm1616_data[12]=kd_vol/100%10;	
			sm1616_data[13]=kd_vol/10%10+18;
			sm1616_data[14]=kd_vol%10;
			break;
		case 4:	 //显示报警电压
			disp_num_pro(bj_vol); 
			break;	
		case 5://显示开机时间
			disp_num_pro(timeingon_temp); 
			break;	
		case 6://显示关机时间
			disp_num_pro(timeingoff_temp);  
			break;	
		case 7://显示高压值
// 		high_vol=(t_test*(74005+vol_adj))/1024;
			disp_num_pro(high_vol);  
			break;
		case 8://显示------
			sm1616_data[10]=16;//-
		  sm1616_data[11]=16;
		  sm1616_data[12]=16;	
		  sm1616_data[13]=16;
		  sm1616_data[14]=16; 
			break;
		case 9:   //显示高压值
// 	    disp_num_pro(high_vol);  
			break;
		default:
			break;
	}	
}
/******************************************************************************************
** 函数名称 ：disp_num_pro()函数
** 函数功能 ：显示处理
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
***************************************************************************************/
void disp_num_pro(unsigned int num)
{
	if(num>=10000)
	{
		sm1616_data[10]=num/10000;
		sm1616_data[11]=num/1000%10;
		sm1616_data[12]=num/100%10;	
		sm1616_data[13]=num/10%10;
		sm1616_data[14]=num%10;
	}
	else if(num>=1000)
	{
		sm1616_data[10]=17;
		sm1616_data[11]=num/1000%10;
		sm1616_data[12]=num/100%10;	
		sm1616_data[13]=num/10%10;
		sm1616_data[14]=num%10;
	}	
	else if(num>=100)
	{
		sm1616_data[10]=17;
		sm1616_data[11]=17;
		sm1616_data[12]=num/100%10;	
		sm1616_data[13]=num/10%10;
		sm1616_data[14]=num%10;
	}	
	else if(num>=10)
	{
		sm1616_data[10]=17;
		sm1616_data[11]=17;
		sm1616_data[12]=17;	
		sm1616_data[13]=num/10%10;
		sm1616_data[14]=num%10;
	}	
	else
	{
		sm1616_data[10]=17;
		sm1616_data[11]=17;
		sm1616_data[12]=17;	
		sm1616_data[13]=17;
		sm1616_data[14]=num%10;
	}		
}
/*==================================================
函数原型：void	Task_Out(void)
函数说明：端口输出
入口参数：无
出口参数：无
==================================================*/
void	Task_Out(void)
{
	if (Tmr_20ms_Fg)
	{
		disp_data_pr(); 
		Display_pr(sm1616_data); 
	}
}