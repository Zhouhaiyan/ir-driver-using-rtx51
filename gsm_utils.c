#include "gsm_common.h"
#include "gsm_utils.h"
#include "string.h"
#include "gsm_port.h"


//void os_try(void){
////	switch(os_wait2(K_SIG|K_IVL,100))
////	{
////		case TMO_EVENT:
////			// 发生了超时，不需要 Os_reset_interval
////			break;
////		
////		case SIG_EVENT:
////	　　
////	　　os_reset_interval(100);// 收到信号，需要 Os_reset_interval 
////	　　/* 依信号执行的其它操作 */
////			break;
////		
////		default:
////			break;
////	}
//}

char gsm_agent(const char * cmd, const char * rsp){
	unsigned char i=0;
	//clr_buf();
	while(!hand(rsp)&&++i<10)                            //判断是否握手成功,如果不成功延时一会,再发送AT握手指令
	{
		GSM_WriteString(cmd);
		os_sleep_ms(500);
	}
	clr_buf();
	if(i>=10)
		return AGSM_RESP_TIMEOUT;
	return AGSM_OK;
}


/******************************************************************************************
** 函数名称 ：hand()函数
** 函数功能 ：判断接收的数据中是否含有字符串
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/
bit hand(unsigned char *a)
{ 
		if(strstr(msg_recv_buf,a)!=NULL)
			return 1;
		else
			return 0;
}
bit hand1(char *p,char *a)
{
	if(strstr((char*)p,(char*)a)!=NULL)
		return 1;
	else 
		return 0;
}



/******************************************************************************************
** 函数名称 ： GSM_WriteString()函数
** 函数功能 ：一直发到00结束 最后不添加
** 入口参数 ： 
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/	
void GSM_WriteString(char *p)	
{
	if(p == 0)
		return;
	
	while(*p)
	{
	  GSM_WriteOneByte(*p);
	  p++;
	}
	
}


/******************************************************************************************
** 函数名称 ： char2hex()函数
** 函数功能 ：char2hex 把字符转换成16进制字符
** 入口参数 ： hex: 16进制字符存储的位置指针，ch：字符
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/	
void char2hex(char *hex,char ch)
{
    code char numhex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    *hex++  = numhex[(ch & 0xF0)>>4];
    *hex    = numhex[ ch & 0x0F];
}



/******************************************************************************************
** 函数名称 ： gbk2ucs2hex()函数
** 函数功能 ：把asc码转为ucs2码
** 入口参数 ：
** 出口参数 ： null
** 作者：gsl
** 修改记录：
输入：188xxxx0763,输出"003100380038...0033"
******************************************************************************************/	
void gbk2ucs2hex(char * ucs2hex,char * gbk)
{
   unsigned char i=0;
   while(* gbk && i<230)
    {
        if((*gbk&0xFF) < 0x7F)      //英文
        {
            
            *ucs2hex++ = '0';
            *ucs2hex++ = '0';
            char2hex(ucs2hex,*gbk);
            ucs2hex+=2;
            gbk++;  
					i++;
        }
    }
    *ucs2hex=0;
}
/******************************************************************************************
** 函数名称 ： hex_c_ucs()函数
** 函数功能 ：十六进制转为ucs码程序
** 入口参数 ： num 需要转换的数字，len转换字节长度
** 出口参数 ： 转换的内容
** 作者：gsl
** 修改记录：
输入0，输出"0000";
输入15，输出"000E"
******************************************************************************************/	
void hex_c_ucs(float num,unsigned char len)
{
	unsigned char xdata temp[10],i;
	unsigned long xdata d_temp=0;
	memset(temp,'\0',15);
	memset(msg_recv_buf,'\0',sizeof(msg_recv_buf));
	d_temp=(num*10);
	if(d_temp%10!=0)//为小数
	{
		temp[0] = d_temp/100 + '0'; 
		temp[1] = d_temp/10%10 + '0';  
		temp[2] = 0x2e;
		temp[3] = d_temp % 10 + '0';
	}
	else
	{
		d_temp=d_temp/10;
		switch (len)
		{
			case 1:
				temp[0]=d_temp+ '0'; 
				break;
			case 2:
				temp[0] = d_temp/10 + '0';  			
				temp[1] = d_temp % 10 + '0'; 							
				break;
			case 3:
				temp[0] = d_temp/100 + '0'; 
				temp[1] = d_temp/10%10 + '0';				
				temp[2] = d_temp%10 + '0'; 							
				break;	
			case 4:
				temp[0] = d_temp/1000 + '0'; 
				temp[1] = d_temp/100%10 + '0';	
				temp[2] = d_temp/10%10 + '0';
				temp[3] = d_temp%10 + '0'; 							
				break;
			case 5:
				temp[0] = d_temp/10000 + '0'; 
				temp[1] = d_temp/1000%10 + '0';	
				temp[2] = d_temp/100%10 + '0';
				temp[3] = d_temp/10%10 + '0';					
				temp[4] = d_temp%10 + '0'; 							
			break;
			case 6:
				temp[0] = d_temp/100000 + '0'; 
				temp[1] = d_temp/10000%10 + '0';	
				temp[2] = d_temp/1000%10 + '0';
				temp[3] = d_temp/100%10 + '0';	
				temp[4] = d_temp/10%10 + '0';
				temp[5] = d_temp%10 + '0'; 							
			break;					
			default:
				break;
		}
	}
	for(i=0;i<10;i++)
	{
			if((temp[i]&0xFF) < 0x7F)      //英文
			{
				if(temp[i]=='\0')
					break;
					msg_recv_buf[4*i] = '0';
					msg_recv_buf[4*i+1] = '0';
					char2hex(msg_recv_buf+(4*i+2),temp[i]);  
			}
	}
}



/******************************************************************************************
** 函数名称 ： clr_buf()函数
** 函数功能 ：清除接收缓存
** 入口参数 ： null
** 出口参数 ： null
** 作者：gsl
** 修改记录：
******************************************************************************************/	
void clr_buf(void)
{
	unsigned char i; 
	Rec_point=0;
	for(i=0;i<=GSM_RX_BUF_SIZE;i++)     
		msg_recv_buf[i]=0;
}





/******************************************************************************************
** 函数名称 ： Task_Guzh_Smg_Hf()函数
** 函数功能 ：故障短信回复处理
** 入口参数 ： Gsm_Tx_Fg 1 执行 0 不执行
** 出口参数 ： Sms_Hf_Fg 回复标志 1 需要回复 0 不需要回复
** 引用：
Gsm_Tx_Fg：输入使能
Hf_Code：1~8；fd_fail fina_fd_flag gsm_time_on_flag gsm_time_off_flag：
选择短信内容
Sms_Hf_Fg：输出，通知发送短信任务。
** 修改记录：
******************************************************************************************/	
void Task_Guzh_Smg_Hf(void)
{ 
	#if 0
	static unsigned char s=0;
	switch (s)
	{
		case 0:
			if (!Gsm_Tx_Fg)//若有回复短信标志为1，则转入回复处理程序
			{
				return;
			}	
			memset(msg_response_buf,'\0',400);				
			clr_buf();
			Gsm_Tx_Fg=0;
			s++;
			break;				
		case 1:
			if(Hf_Code==1) 	//压线保护					
			{
				strcat((char*)msg_response_buf,(char*)str_txn_hf);  //提醒您—
				strcat((char*)msg_response_buf,(char*)str_yx_hf); //压线保护
			}	
			if(Hf_Code==2) 	//匮电保护					
			{			
				strcat((char*)msg_response_buf,(char*)str_kd);  //匮电
				strcat((char*)msg_response_buf,(char*)str_bh_hf); //保护
			}	
			if(Hf_Code==3) 	//超压保护					
			{			
				strcat((char*)msg_response_buf,(char*)str_cy);  //超压
				strcat((char*)msg_response_buf,(char*)str_bh_hf); //保护
			}	
			if(Hf_Code==4) 	//高温保护					
			{			
				strcat((char*)msg_response_buf,(char*)str_gw);  //高温
				strcat((char*)msg_response_buf,(char*)str_bh_hf); //保护
			}			
			if(Hf_Code==5) 	//故障保护					
			{			
				strcat((char*)msg_response_buf,(char*)str_gzh);  //故障
				strcat((char*)msg_response_buf,(char*)str_bh_hf); //保护
			}	
			if(Hf_Code==7) 	//放电人模式回复			
			{		
				strcat((char*)msg_response_buf,(char*)str_fdr_hf); 
			}			
			if(fd_fail==1) 	//放电失败或未安装自动放电设备》
			{	
				fd_fail=0;
				strcat((char*)msg_response_buf,(char*)str_khl); 
				strcat((char*)msg_response_buf,(char*)str_fd_hf);  //放电
				strcat((char*)msg_response_buf,(char*)str_fd_fail);  //失败
				strcat((char*)msg_response_buf,(char*)str_hwazhfdsb);  //或未安装自动放电设备》
				strcat((char*)msg_response_buf,(char*)str_khr);
			}	
			if(fina_fd_flag==1) 	//放电完成回复				
			{	
				fina_fd_flag=0;		
				strcat((char*)msg_response_buf,(char*)str_end_hf);  //放电完成 机器安全状态
			}	
			
			if(gsm_time_on_flag==1) 	//定时关机回复		
			{		
				gsm_time_on_flag=0;
				strcat((char*)msg_response_buf,(char*)str_txn_hf); //提醒您—
				strcat((char*)msg_response_buf,(char*)str_ds_hf);  //定时
				strcat((char*)msg_response_buf,(char*)str_on);  //关
				strcat((char*)msg_response_buf,(char*)str_ji);  //机
				strcat((char*)msg_response_buf,(char*)str_fdrbh_succes);  //成功	
					 
			}	
			if(gsm_time_off_flag==1) 	//定时关机回复		
			{		
				gsm_time_off_flag=0;			
				strcat((char*)msg_response_buf,(char*)str_txn_hf); //提醒您—
				strcat((char*)msg_response_buf,(char*)str_ds_hf);  //定时
				strcat((char*)msg_response_buf,(char*)str_off);  //关
				strcat((char*)msg_response_buf,(char*)str_ji);  //机
				strcat((char*)msg_response_buf,(char*)str_fdrbh_succes);  //成功	
			}
			if(Hf_Code==8)		
			{
			// 					call_count=0;
				call_flag=0;
				strcat((char*)msg_response_buf,(char*)str_stop);  //停机电压值
				strcat((char*)msg_response_buf,(char*)str_dy_hf);
				strcat((char*)msg_response_buf,(char*)str_mh);		
				hex_c_ucs(stop_vol,5);
				strcat((char*)msg_response_buf,(char*)msg_recv_buf); //高压值
				strcat((char*)msg_response_buf,(char*)str_v); //高压值			
				strcat((char*)msg_response_buf,(char*)str_enter); //换行

				strcat((char*)msg_response_buf,(char*)str_fd_hf);  //放电电压值
				strcat((char*)msg_response_buf,(char*)str_dy_hf);
				strcat((char*)msg_response_buf,(char*)str_mh);	
				if(chd_base_vol<stop_vol)hex_c_ucs(stop_vol-chd_base_vol,5);
				else hex_c_ucs(0,2);
				strcat((char*)msg_response_buf,(char*)msg_recv_buf); //高压值
				strcat((char*)msg_response_buf,(char*)str_v); //高压值			
				strcat((char*)msg_response_buf,(char*)str_enter); //换行		

				strcat((char*)msg_response_buf,(char*)str_shy);  //剩余电压值
				strcat((char*)msg_response_buf,(char*)str_dy_hf);
				strcat((char*)msg_response_buf,(char*)str_mh);
				hex_c_ucs(chd_base_vol,5);
				strcat((char*)msg_response_buf,(char*)msg_recv_buf); //高压值
				strcat((char*)msg_response_buf,(char*)str_v); //高压值			
				strcat((char*)msg_response_buf,(char*)str_enter); //换行			
				strcat((char*)msg_response_buf,(char*)str_l); //量	
				
				strcat((char*)msg_response_buf,(char*)str_mh); //：
				if(chd_base_vol<stop_vol)	
				{
					chd_base_vol=stop_vol-chd_base_vol;
					chd_base_vol=(unsigned int)((chd_base_vol)/(stop_vol/100));
				  hex_c_ucs(chd_base_vol,2);
				}
				else hex_c_ucs(0,2);
				strcat((char*)msg_response_buf,(char*)msg_recv_buf); //放电量百分比
				strcat((char*)msg_response_buf,(char*)str_bfh); //次 
			}	
			memset(send_num,'\0',12);
			strcat((char*)send_num,(char*)num_bound); //把绑定号码复制到要发送的号码寄存器中
			Hf_Code=0;
			s=0;
			Sms_Hf_Fg=1;		
			break;			
			
	    default:
			clr_buf();
			s=0;
			break;
	}
	
	#endif
}

