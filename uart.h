#ifndef __UART_H__
#define __UART_H__


void Uart_Init(void);
unsigned char Uart_SendByte(unsigned char);
void Uart_SendByteN(unsigned char *d, unsigned short len);
void Uart_PrintHex(unsigned char a);

#endif