
#include "user_config.h"
#if USE_OS
#include "rtx51tny.h"

#include "STC15F2K60S2.h"//单片机头文件,24MHz时钟频率
#include "SIM900A.H"
#include "uart.h"
#include "IR.H"
#include "TM1640.h"
#include "os_api.h"
#include "gsm_port.h"
//https://blog.csdn.net/dkr269944905/article/details/72822979
//https://wenku.baidu.com/view/6409b91a453610661ed9f465.html
//https://www.cnblogs.com/jieruishu/p/9291214.html


void main_job() _task_ 0   
{
	int i;
	AUXR |= 0x80;//定时器0工作在1T模式.否则OS延时不准。
	ir_init();//
	Uart_Init();
	TM1640_Init(0);
	while(1){
		i = sizeof(msg_recv_buf);
		for(;i>0;i--){
			
		SBUF = '~';
		OS_DELAY(2000);
		}
	}
	os_create_task(1);//task1   
//	os_create_task(2);//task2  
	
	os_delete_task(0);//delete task0  
}   
  
void led0_job() _task_ 1    
{	
	for (;;)   	
	{

//		P31 = ~P31;
//		os_wait(K_TMO,400/SysTickMS,0);
//		
		test_gsm();
//		/*关于os_wait函数的理解第一个参数为等待时间，有三个选项K_SIG(启动信号)，K_TMO(超时信号)，K_IVL(周期信号)或是组合使用。
//		第二个参数为等待的滴答时间，设置为10的话就是基准时间片*10，第三个参数未使用，默认设置为0*/
		
	}
}
  

void led1_job() _task_ 2    
{
	for (;;)
	{
		P31 = 0;
//		os_wait(K_TMO,100,0);
//		os_switch_task();
	}
}
  

#endif