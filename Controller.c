
#include 	<stdlib.h> 
#include 	<stdio.h>
#include	<string.h>
#include "apl.h"

/* Includes ------------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

static void play_configu16(config_u16_t *cfg, uint8_t *arg_list){
	
	#define value	(cfg->value)
	#define step	(cfg->step)
	#define min		(cfg->min)
	#define max		(cfg->max)
	#define unit	(cfg->unit)
	#define op		(arg_list[0])
	uint16_t old_value = value;
	if(arg_list == 0)
		return;
	switch(op){
		case OPC_SET:
			value = (uint16_t)arg_list[1] << 8 + arg_list[2];
			break;
		case OPC_INC:
			value = value + step;
			break;
		case OPC_DEC:
			value = value - step;
			break;
	}
	if(value < min || value > max)
		value = old_value;	
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
Event_t PollC(Event_t e)
{
	uint8_t * pu8;
	uint8_t evt_id;
	if(!e)
		return 0UL;
	
	if(e&EVT_MASK_GSM_Bind){
		evt_id = EVT_GSM_Bind;
		app.msg_buf[evt_id][0];
		app.msg_buf[evt_id][1];//~N。绑定输入
		if(1){
			app.m.bind_status = 1;//绑定成功
		}
		app.v.msg |= EVT_MASK_GSM_Bind;
		return e^EVT_MASK_GSM_Bind;
	}

	if(e&EVT_MASK_Power){
		evt_id = EVT_Power;
		switch(app.msg_buf[evt_id][0]){
			case OPC_SET:
				app.m.power = PWR_ON;
				break;
			case OPC_RESET:
				app.m.power = PWR_OFF;
				break;
			case OPC_ASSIGN:
				app.m.power = app.msg_buf[evt_id][1]%PWR_VALIDE;//值只能是0~PWR_VALIDE-1
				break;
		}
		app.v.msg |= EVT_MASK_Power;
		return e^EVT_MASK_Power;
	}

	if(e&EVT_MASK_AntiShock){
		evt_id = EVT_AntiShock;
		if(app.m.power != PWR_OFF){
			switch(app.msg_buf[evt_id][0]){
				case OPC_TGL:
					app.m.anti_shock_sw = !app.m.anti_shock_sw;
					break;
				case OPC_SET:
					app.m.anti_shock_sw = 1;
					break;
				case OPC_RESET:
					app.m.anti_shock_sw = 0;
					break;
				case OPC_ASSIGN:
					app.m.anti_shock_sw = !!app.msg_buf[evt_id][1];//值只能是0/1
					break;
			}
			app.v.msg |= EVT_MASK_AntiShock;
		}
		return e^EVT_MASK_AntiShock;
	}

	if(e&EVT_MASK_SwScreen){
		evt_id = EVT_SwScreen;
		if(app.m.power != PWR_OFF){
			switch(app.msg_buf[evt_id][0]){
				case OPC_TGL:
					app.m.screen_switch = !app.m.screen_switch;
					break;
				case OPC_SET:
					app.m.screen_switch = 1;
					break;
				case OPC_RESET:
					app.m.screen_switch = 0;
					break;
				case OPC_ASSIGN:
					app.m.screen_switch = !!app.msg_buf[evt_id][1];//值只能是0/1
					break;
			}
			app.v.msg |= EVT_MASK_SwScreen;
		}
		return e^EVT_MASK_SwScreen;
	}

	if(e&EVT_MASK_SwTiming){
		evt_id = EVT_SwTiming;
		switch(app.msg_buf[evt_id][0]){
			case OPC_TGL:
				app.m.timing_switch = !app.m.timing_switch;
				break;
			case OPC_SET:
				app.m.timing_switch = 1;
				break;
			case OPC_RESET:
				app.m.timing_switch = 0;
				break;
			case OPC_ASSIGN:
				app.m.timing_switch = !!app.msg_buf[evt_id][1];//值只能是0/1
				break;
		}
//		app.v.msg |= EVT_MASK_SwTiming;
		
		return e^EVT_MASK_SwTiming;
	}

//	if(e&EVT_MASK_GSM_Inquiry){
//		return e^EVT_MASK_GSM_Inquiry;
//	}

	
	#define __do_cfgx__(event_id)	do{\
																	play_configu16(&app.m.unconfigs.se_array[event_id-EVT_CFG1],app.msg_buf[event_id]);\
																	if(app.v.CurView & Screen4View)\
																		app.v.msg |= 1UL<<event_id;\
																}while(0)
	
	if(e&EVT_MASK_CFG1){
		__do_cfgx__(EVT_CFG1);
		return e^EVT_MASK_CFG1;
	}

	if(e&EVT_MASK_CFG2){
		__do_cfgx__(EVT_CFG2);
		return e^EVT_MASK_CFG2;
	}

	if(e&EVT_MASK_CFG3){
		__do_cfgx__(EVT_CFG3);
		return e^EVT_MASK_CFG3;
	}

	if(e&EVT_MASK_CFG4){
		__do_cfgx__(EVT_CFG4);
		return e^EVT_MASK_CFG4;
	}

	if(e&EVT_MASK_CFG5){
		__do_cfgx__(EVT_CFG5);
		return e^EVT_MASK_CFG5;
	}

	if(e&EVT_MASK_CFG6){
		__do_cfgx__(EVT_CFG6);
		return e^EVT_MASK_CFG6;
	}

	if(e&EVT_MASK_CFG7){
		__do_cfgx__(EVT_CFG7);
		return e^EVT_MASK_CFG7;
	}

	if(e&EVT_MASK_CFG8){
		__do_cfgx__(EVT_CFG8);
		return e^EVT_MASK_CFG8;
	}

	if(e&EVT_MASK_CFG9){
		__do_cfgx__(EVT_CFG9);
		return e^EVT_MASK_CFG9;
	}

	if(e&EVT_MASK_GSM_Unbind){
		
		evt_id = EVT_GSM_Unbind;
		app.msg_buf[evt_id][0];
		app.msg_buf[evt_id][1];//~N。绑定输入
		if(1){
			app.m.bind_status = 0;//解绑成功
		}
		app.v.msg |= EVT_MASK_GSM_Bind;
		return e^EVT_MASK_GSM_Unbind;
	}

//	if(e&EVT_MASK_Temperature){//an internal event
//		return e^EVT_MASK_Temperature;
//	}

//	if(e&EVT_MASK_BatteryVolt){//an internal event
//		return e^EVT_MASK_BatteryVolt;
//	}

//	if(e&EVT_MASK_HighVolt){//an internal event
//		return e^EVT_MASK_HighVolt;
//	}

//	if(e&EVT_MASK_ShockCounter){//an internal event
//		return e^EVT_MASK_ShockCounter;
//	}

//	if(e&EVT_MASK_Timing){//an internal event
//		return e^EVT_MASK_Timing;
//	}

	if(e&EVT_MASK_SwBuzzer){
		evt_id = EVT_SwBuzzer;
		switch(app.msg_buf[evt_id][0]){
			case OPC_TGL:
				app.m.buzzer_switch = !app.m.buzzer_switch;
				break;
			case OPC_SET:
				app.m.buzzer_switch = 1;
				break;
			case OPC_RESET:
				app.m.buzzer_switch = 0;
				break;
			case OPC_ASSIGN:
				app.m.buzzer_switch = !!app.msg_buf[evt_id][1];//值只能是0/1
				break;
		}
		//do not need notifying View here.
		return e^EVT_MASK_SwBuzzer;
	}

	if(e&EVT_MASK_ClrTiming){
		//@todo: clear timing configuration!!!
		
		
		if(app.v.CurView & Screen3View)
			app.v.msgScreen3 |= EVT_MASK_ClrTiming;
		return e^EVT_MASK_ClrTiming;
	}
//	以下事件属于内部事件，且需要M通过GSM上报
//	if(e&EVT_MASK_Shock){
//		return e^EVT_MASK_Shock;
//	}

//	if(e&EVT_MASK_TouchLine){
//		return e^EVT_MASK_TouchLine;
//	}

//	if(e&EVT_MASK_OverCurrent){
//		return e^EVT_MASK_OverCurrent;
//	}

//	if(e&EVT_MASK_BatteryLow){
//		return e^EVT_MASK_BatteryLow;
//	}

//	if(e&EVT_MASK_OverHeat){
//		return e^EVT_MASK_OverHeat;
//	}

//	if(e&EVT_MASK_AntiShock){
//		return e^EVT_MASK_AntiShock;
//	}
	return e;
}
