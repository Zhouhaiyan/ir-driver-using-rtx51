#ifndef __GSM_UTILS_H__
#define __GSM_UTILS_H__

#define AGSM_OK						0
#define AGSM_RESP_TIMEOUT	-1


char gsm_agent(const char * cmd, const char * rsp);
void clr_buf(void);//清除串口接收缓存

bit hand1(char *p,char *a);
bit hand(unsigned char *a);//判断接收的数据中是否含有字符串
void hex_c_ucs(float num,unsigned char len);
void gbk2ucs2hex(char * ucs2hex,char * gbk);
void char2hex(char *hex,char ch);




#endif