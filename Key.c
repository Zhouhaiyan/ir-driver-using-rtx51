#include "key.h"


#define READ_KEY(X,Y)    1

static uint16_t cnt[NbrOfhwSwitch];
static uint8_t idle[NbrOfhwSwitch];
void Key_Init(void)
{	
//  GPIO_InitWrapper(TPin1_PIN,GPIO_Speed_50MHz,GPIO_Mode_IPU);
//  GPIO_InitWrapper(TPin2_PIN,GPIO_Speed_50MHz,GPIO_Mode_IPU);
//  GPIO_InitWrapper(TPin3_PIN,GPIO_Speed_50MHz,GPIO_Mode_IPU);
	int i;
	for(i=0;i<(NbrOfhwSwitch);i++){
		idle[i] = KEY_IDLE;
	}
	for(i=0;i<(NbrOfhwSwitch);i++){
		cnt[i] = 0;
	}
}

static uint32_t HARD_KEY = NO_KEY;
uint32_t TakeKey(void){
  uint32_t k;  
	k = HARD_KEY;
	HARD_KEY = NO_KEY;
  return k;
}

#define SHORT_PRESS_TIME        1
#define LONG_PRESS_TIME         (3000L/KEY_POLL_INTV)
#if 0
static Port_t Switches[NbrOfhwSwitch]={{TPin1_PIN},{TPin2_PIN},{TPin3_PIN},{TPin4_PIN}};
#endif
void ScanKey( void ){
	
	#if 0
	uint8_t i;
	for(i=0;i<NbrOfhwSwitch;i++){
		if(idle[i] == !!READ_KEY(Switches[i].GPIOx,Switches[i].GPIO_Pin)){
				if(cnt[i]){
					if(cnt[i] <  LONG_PRESS_TIME){//is short press
									HARD_KEY |= 1<<i;
					}
					cnt[i] = 0;
				}
		}
		else{//active key
				cnt[i]++;
//				GPIO_TglBits(IO_Probe);
				if(cnt[i]){
					if(cnt[i] == LONG_PRESS_TIME){//is long press
							HARD_KEY |= LONG_KEY<<i;
					}
				}
		}//end of if... else ...
	}//end of for
	
	#endif
}//end of function
