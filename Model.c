
#include 	<stdlib.h> 
#include 	<stdio.h>
#include	<string.h>
#include "apl.h"
#include "os_api.h"
#include "user_config.h"

/* Includes ------------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
#define OpenAntiShockRelay()	P23=1
#define CloseAntiShockRelay()	P23=0
#define IsShockAlarm()	(P44==0)

#define OpenFanRelay()	P21=1
#define CloseFanRelay()	P21=0

#define OpenInverterRelay()		P41=1
#define CloseInverterRelay()	P41=0

/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

static void timing_on_and_timing_off(void){
	static uint32_t SysSecond0 = 0;
	static uint32_t counter = 0;
	static uint8_t power0 = 0xff;
	uint32_t xon,xoff;
	
	xon = app.m.unconfigs.stconfigs.se4_periodical_on_time.value;
	xoff = app.m.unconfigs.stconfigs.se5_periodical_off_time.value;

	if(power0 != app.m.power){
		power0 = app.m.power;
		counter = 0;//每次开关机后，重置定时计数器。
	}
	
	if(SysSecond0 != SysSecond){
		SysSecond0 = SysSecond;
		if(xon && (app.m.power == PWR_OFF)){//关机时，看定时开机有效
			counter ++;
			if( SysSecond0 % (xon) == xon - 1 ){
				app.m.power = PWR_ON;//定时开机时间到，开机。
			}
		}
		else if(xoff && (app.m.power != PWR_OFF)){//开机时，看定时关机有效
			counter ++;
			if( SysSecond0 % (xoff) == xoff - 1 ){
				app.m.power = PWR_OFF;//定时关机时间到，关机。
			}
		}
		else{//如果定时开机、定时关机都无效
			counter = 0;
		}
	}
}

static void monitor_battery_voltage(void){

	static char s = 0;
	switch(s){
		case 0:
			if(app.m.battery_voltage_100mv < app.m.unconfigs.stconfigs.se8_battery_low_voltage_100mv.value){
				s = 1;
				
				//②电瓶低于电瓶电压低于设定值SE-8时，关机、打开指示灯并短信提示【匮电保护-请检查！】
				/*
				 * 生成馈电保护事件
				 */
				if(app.m.power != PWR_OFF){
					/* 1. 关机 --------------------------------------------------- */
					app.m.power = PWR_OFF;//关机
					/* 2. 通知GSM发送短信：【匮电保护-请检查！】 ----------------- */
					app.v.msgGSM |= EVT_MASK_BatteryLow;//通知GSM发送短信
					app.v.msg |= EVT_MASK_BatteryLow;
					/* 3. 打开指示灯 --------------------------------------------- */
					app.msg_buf[EVT_BatteryLow][0] = OPC_SET;//通知点灯
				}
			}
			break;
		case 1:
			if(app.m.battery_voltage_100mv > 50 + app.m.unconfigs.stconfigs.se8_battery_low_voltage_100mv.value){
				s = 0;
				
				app.v.msg |= EVT_MASK_BatteryLow;
				app.msg_buf[EVT_BatteryLow][0] = OPC_RESET;//通知灭灯
			}
			break;		
	}
}
static void detect_shock_touchline_overcurrent(void){
	static uint8_t s = 0;
	static uint32_t SysTime0 = 0;

	//开机状态下放电达到报警电流值se-2时，立即记录超过此电流值的放电时间并且延时判断如下：
	//一丶若超过SE-2设定值的强放电时间未超过压线保护设定值SE-3,则立即向绑定者拨号报警10秒自动挂机，挂机5秒后发送短信，内容为：       
	//①高压电压：12000V    
	//②电瓶电压：52V   
	//③放电时间：5秒   
	//④放电功率：60000瓦 （功率计算方法为SE-1设定的高压值乘以放电时间）
	//⑤放电次数：1次  
	//二、若超SE-2报警电流值的强放电时间超过压线保护设定值se-3，则立即关机并且点亮压线保护指示灯同时向绑定者发送短信：压线保护-请检查！
	if(app.m.power != PWR_OFF){
		#define IS_IDLE_CURRENT(i)	(i < app.m.unconfigs.stconfigs.se2_shock_current.value)
		#define IS_TOUCH_or_SHOCK_CURRENT(i)	(i < app.m.unconfigs.stconfigs.se9_over_current.value && i>= app.m.unconfigs.stconfigs.se2_shock_current.value)
		#define IS_OVRLD_CURRENT(i)	(i>= app.m.unconfigs.stconfigs.se9_over_current.value)
		#define IS_LONGER_THAN_SE3TIME(t)	((t) >= app.m.unconfigs.stconfigs.se3_touch_line_protect_time.value)
		switch(s){
			case 0:
				if(IS_IDLE_CURRENT(0)){//小于报警电流设定值se-2,认为是待机漏电流。
				}
				else if(IS_TOUCH_or_SHOCK_CURRENT(0)){//达到报警电流设定值se-2，需要延时se-3时间，区分是放电还是触线。
					s = 1;
					SysTime0 = SysTime;
				}
				else if(IS_OVRLD_CURRENT(0)){//达到过载电流设定值se-9，认为是过流。
					s = 2;
				}
				break;
			case 1://检测电流大小、持续时间。
				
				if(IS_IDLE_CURRENT(0)){//小于报警电流设定值se-2,认为是放电结束。
					s = 0;
					/*
					 * 生成“放电”事件
					 */
					/* 1. 记录放电时间（毫秒） ----------------------------------- */
					app.m.shock_time_ms = (uint8_t)(SysTime - SysTime0);
					/* 2. 更新放电次数 ------------------------------------------- */
					app.m.shock_counter ++;
					/* 3. 通知GSM上报放电事件 ------------------------------------ */
					app.v.msgGSM |= EVT_MASK_Shock;
					/* 4. 刷新数码管显示 ----------------------------------------- */
					if(app.v.CurView & Screen1View)
						app.v.msgScreen1 |= EVT_MASK_Shock;//如果1#界面激活，通知1#界面刷新报警计数显示
					/* 5. 通知蜂鸣器 ------------------------------------ */
					if(app.m.buzzer_switch){
						//打到东西报警几秒，关闭了就不报警。
						app.v.msg |= EVT_MASK_Beep;
						app.msg_buf[EVT_Beep][0] = OPC_SET;
					}
				}
				else if(IS_TOUCH_or_SHOCK_CURRENT(0)){//达到报警电流设定值se-2，需要延时se-3时间，区分是放电还是触线。
					
					if(IS_LONGER_THAN_SE3TIME((uint16_t)(SysTime - SysTime0)) ){//达到压线保护时间，认为是压线.
						app.v.msgGSM |= EVT_MASK_TouchLine;
						/*
						 * 生成“压线保护”事件
						 */
						/* 1. 关机 ------------------------------------------------- */
						app.m.power = PWR_OFF;
						//todo: 通知GSM等
						
						s = 0;
					}
				}
				else if(IS_OVRLD_CURRENT(0)){//达到过载电流设定值se-9，认为是过流。
					s = 2;
				}
				break;
			case 2:
				/*
				 * 生成“过流保护”事件
				 */
				/* 1. 关机 ------------------------------------------------- */
				app.m.power = PWR_OFF;
				/* 2. GSM通知用户 ------------------------------------------ */
				app.v.msgGSM |= EVT_MASK_OverCurrent;
				s = 0;
				break;
		}
	}
}





static void InverterTask() _task_ TASK_ID_InverterTask {
	#define RELAY_ON_TIME_MS	(app.m.unconfigs.stconfigs.se4_periodical_on_time.value * 200)
	#define RELAY_OFF_TIME_MS	(app.m.unconfigs.stconfigs.se4_periodical_off_time.value * 200)
	while(1){
		OpenInverterRelay();
		OS_DELAY(RELAY_ON_TIME_MS);
		CloseInverterRelay();
		OS_DELAY(RELAY_OFF_TIME_MS);
	}
}
/**
  * @brief  This function handles Model events.
  * @param  None
  * @retval None
  */
void PollM(void)
{
	
	/* 1. 开关机 ----------------------------------- */
	static uint8_t power0 = 0xff;
	if(power0 != app.m.power){
		
		os_delete_task(TASK_ID_InverterTask);//只要开关机状态有变化，都先删除交流机任务
		if(app.m.power != PWR_OFF){//开机
			//打开指示灯->通知显示层
			//打开数码管->通知显示层
			app.m.screen_switch = 1;
			app.v.msg |= EVT_MASK_SwScreen;
			
			//打开交流机。分为两种情况：一直打开，和间歇打开
			OpenInverterRelay();//一直打开
			if(app.m.power == PWR_SAVING){//间歇打开
				os_create_task(TASK_ID_InverterTask);//创建交流机任务
			}
		}
		else{//关机
			//关闭指示灯->通知显示层
			app.m.screen_switch = 0;
			app.v.msg |= EVT_MASK_SwScreen;
			//关闭数码管->通知显示层
			//关闭蜂鸣器->通知显示层
			//关闭交流机
			CloseInverterRelay();
			//关闭防电人副机
			CloseAntiShockRelay();
			//风扇不由开关机控制，受温度控制
		}
		power0 = app.m.power;
	}
	
	/* 2. 定时开关机 ------------------------------- */
	timing_on_and_timing_off();
	
	/* 3. 防电人功能 ------------------------------- */
	//防电人副机继电器2p:开启防电人功能时此继电器长开，否则常闭
	if(app.m.power != PWR_OFF){
		if(app.m.anti_shock_sw){
			//打开防电人副机继电器2p,让防电人副机开始工作
			OpenAntiShockRelay();
			if(IsShockAlarm()){//读防电人副机12V输入。如果为高电平，则关机。
//			④开启防电人功能状态下有防人信号输入则立即关机并短信提醒【有人来了-已提前关机至安全状态！】
				/*
				 * 生成防电人事件，并通过GSM通知用户
				 */
				
				/* 1. 关机 --------------------------------------------------- */
				app.m.power = PWR_OFF;//关机
				
				/* 2. 通知GSM上报防电人事件 ---------------------------------- */
				app.v.msgGSM |= EVT_MASK_AntiShock;
			}
		}
		else{
			//关闭防电人副机继电器2p
			CloseAntiShockRelay();
		}
	}
	
	/* 4. 放电、压线、过流监测 --------------------- */
	detect_shock_touchline_overcurrent();
	
	/* 5. 检测温度 --------------------------------- */
	if(app.m.temperature > 60){
		//打开风扇
		OpenFanRelay();
		
		//③温度超过60℃则关机并短信提醒：【超温保护-请检查】，同时关机灯快闪
		/*
		 * 生成一次超温保护事件
		 */
		if(app.m.power != PWR_OFF){
			/* 1. 关机 --------------------------------------------------- */
			app.m.power = PWR_OFF;
			
			/* 2. 通知GSM上报超温保护事件 -------------------------------- */
			app.v.msgGSM |= EVT_MASK_OverHeat;
			
			/* 3. 通知显示模块，让关机灯快闪 ----------------------------- */
			app.v.msg |= EVT_MASK_OverHeat;
		}
	}
	else if(app.m.fan_switch < 55){
		//关闭风扇
		CloseFanRelay();
	}
	
	/* 6. 监测电瓶 --------------------------------- */
	monitor_battery_voltage();
}
